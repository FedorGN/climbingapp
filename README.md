# climbing-FastApi-ReactJS

## About project

Climbing application with user registration.

Backend is built on FastAPI framework and running on Uvicorn ASGI web server. Data Base is PostgreSQL  in the docker container.

There is a fake smtp server (MailHog). Set ENV variables to use MailHog to intercept and view sent mails.

FrontEnd is built with ReactJS fonctional components and react hooks. Styles and components are BootStrap ReactJS components.

Servers are running inside docker containers. Docker-compose is used to manage them.

Docker volumes are binded with developer workstations for direct code modifications. Hot reload are supported for climbing-api (FastAPI) and climbing-frontend (ReactJS) containers.

Stack:
- FastAPI
- ReactJS, Bootstrap
- PostgreSQL
- Celery, Redis, Flower
- SQLite
- MailHog
- Nginx, Certbot
- Docker and Docker-Compose

```mermaid
graph TD;

  User-->Nginx[Reverse proxy Nginx]

  subgraph " "
  Nginx-->FastAPI;
  FastAPI-->Redis[Message broker Redis]
  Redis-->Celery[Worker Celery]
  Flower-->Redis
  Nginx-->Flower
  Nginx-->ReactJS;
  Nginx-->MailHog;
  Nginx-->PostgreSQL;
  end
```

## Getting started

1. Create a copy .env.exemple and rename it to .env. Configure .env if needed. .env is file with environment variables used for project configuration.
2. Create all application containers running the file "start-docker-linux.sh"

Once launched, there will be available hosts:
- http://localhost:8081/ - FrontEnd application
- http://localhost:8083/docs - BackEnd Swagger documentions
- http://localhost:8084 - Flower to view Celery Tasks
- http://localhost:8085/ - MailHog to view mails sent by application
- http://localhost:8086/ - PGAdmin


## Some useful commands

Build (or rebuild) the containers and launch all services:
```
docker-compose up --build --force-recreate
```

Stop all services:

```
docker-compose down
```
Launch a new instance of the docker-compose service using docker-compose (starting a new container and running /bin/sh inside). This is useful for container debugging:
```
docker-compose run climbing-api /bin/sh
```
Execute a command inside a running container for service climbing-my-service (here are a running a /bin/sh inside climbing-api container):
```
docker-compose exec climbing-api /bin/sh
```
Read a file from a container (to list all docker containers run docker ps -a):
```
docker container cp dadd0a5984de5c7f1dac6300a284decf3cad897180c370b8ecab647d3202fd43:/app/package.json -
```
Run tests inside climbing-api container:
```
docker-compose exec climbing-api python -m pytest tests/
```
Run shell inside FastAPI container:
```
docker-compose exec climbing-api /bin/sh
```
Restart docker container in command line (in this exemple worker is climbing-api):
```
docker-compose restart climbing-api
```
