import { Container, Row, Col, Card } from "react-bootstrap";
import { Link } from "react-router-dom"

import { RouterPath } from "../../assets/dictionary/RouterPath";

export default function HomePage(props) {
  return (
    <>
      <Container className="d-flex flex-column min-vh-90">
        <Row className="justify-content-center pt-5 ">
          <Col xs={12} sm={12} md={10} lg={8} xl={6} >
            <Card>
              <Card.Header as="h5">Welcome to Climbing App Roc14!</Card.Header>
              <Card.Body>
                <Card.Text>
                  <p>Sign up and profit of all functionnalities!</p>
                  <ul>
                    <li>See all available routes, their grades and description</li>
                    <li>Mark the route you sent</li>
                    <li>Follow your friends to see their progress</li>
                  </ul>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row className="d-flex h-100 flex-column justify-content-end row">
          <Col className="d-flex mt-3 flex-column justify-content-end align-items-center col">
            <p>2022 - Climbing App. <Link className="text-decoration-none" to={RouterPath.ABOUT_PAGE}>About</Link>.</p>
          </Col>
        </Row>
      </Container>
    </>
  );
}
