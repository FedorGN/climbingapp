import { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { Button, Container, Row, Col, Card } from "react-bootstrap";
import BootstrapTable from 'react-bootstrap-table-next';
import ReactPaginate from "react-paginate"

import DataService from "./RoutesService";
import FiltersRoutes from "./filters_routes/FiltersRoutes"
import {
  formatterRouteName, formatterRouteLine, formatterRouteGrade
} from "../../../assets/utils/formatters/TableFormatters"

export default function RoutesList() {

  let [searchParams, setSearchParams] = useSearchParams();
  const [ItemsRoutes, setItemsRoutes] = useState([]);
  const [pageCount, setpageCount] = useState(0);
  const [newPage, setnewPage] = useState(searchParams.get("page") || 1);
  const [isLoading, setisLoading] = useState(true);

  const itemsPerPage = 20;

  const handleDone = (e, uuid) => {
    e.preventDefault();
    var data = {
      "uuid_route": uuid,
      "is_done": true
    };
    DataService.postUpdateUserRoute(data)
      .then((response) => {
        if (response.status === 201) {
          let items = [...ItemsRoutes]
          for (var i = 0; i < items.length; i++) {
            if (items[i]["uuid"] === uuid) {
              items[i].id_user_route = 1
              break;
            }
          }
          setItemsRoutes(items)
        }
      })
      .catch((error) => {
        // setisSendingRequest(false);
      });
  }

  const handleClearFilters = () => {
    setSearchParams({});
    setnewPage(1)
  };


  function FormatterDone(
    cell,
    row,
    rowIndex,
    formatExtraData
  ) {
    if (formatExtraData[rowIndex].id_user_route) {
      return (
        <div>Yes</div>
      );
    }
    else {
      return (
        <Button variant="primary"
          type="submit"
          size="sm"
          onClick={(e) => handleDone(e, formatExtraData[rowIndex].uuid)}
        >{"I sent it!"}</Button>
      )
    }
  }

  const columns = [{
    dataField: 'line_name',
    text: 'Line',
    formatter: formatterRouteLine,
    formatExtraData: ItemsRoutes
  }, {
    dataField: 'route_name',
    text: 'Name',
    formatter: formatterRouteName,
    formatExtraData: ItemsRoutes
  }, {
    dataField: 'grade',
    text: 'Grade',
    formatter: formatterRouteGrade,
    formatExtraData: ItemsRoutes
  }, {
    dataField: 'id_user_route',
    text: 'Done',
    formatter: FormatterDone,
    formatExtraData: ItemsRoutes,
    align: 'right',
    headerAlign: 'right',
  }];

  useEffect(() => {
    setisLoading(true)
    setnewPage(searchParams.get("page") || 1)
    DataService.getRoutes(
      searchParams.get("name"), searchParams.get("grade"), searchParams.get("line"),
      null, searchParams.get("page"), itemsPerPage)
      .then(
        response => {
          setItemsRoutes(response.data.items)
          setpageCount(Math.ceil(response.data.total / itemsPerPage));
          setisLoading(false)
        }
      )
      .catch((error) => {
        // console.log("error")
      });
  }, [searchParams]);

  const handlePageClick = (event) => {
    let updatedSearchParams = new URLSearchParams(searchParams.toString());
    updatedSearchParams.set('page', event.selected + 1);
    setSearchParams(updatedSearchParams.toString());
    setnewPage(event.selected + 1);
  };

  if (isLoading) { return (<></>) }

  return (
    <>
      <Container>
        <Row className="justify-content-center pt-5 ">
          <Col xs={12}>
            <Card>
              <Card.Body>
                <Card.Title>
                  <Container fluid className="p-0">
                    <Row>
                      <Col xs={6}>Routes</Col>
                      <Col xs={6} className="d-flex justify-content-end gap-2">
                        <Button
                          size="sm"
                          onClick={handleClearFilters}
                          variant="dark"
                          disabled={
                            !(searchParams.has("line") || searchParams.has("grade") || searchParams.has("name"))
                          }>
                          Clear filters
                        </Button>
                        <FiltersRoutes />
                      </Col>
                    </Row>
                  </Container></Card.Title>
                <BootstrapTable bordered={false} hover keyField='id' data={ItemsRoutes} columns={columns} />
                <ReactPaginate
                  className="pagination justify-content-center"
                  nextLabel=">"
                  onPageChange={handlePageClick}
                  forcePage={newPage ? newPage - 1 : 0}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={pageCount}
                  previousLabel="<"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
