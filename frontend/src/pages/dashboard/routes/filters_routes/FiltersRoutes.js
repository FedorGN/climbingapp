import { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { Button, Modal, Form} from "react-bootstrap";

import DataService from "./FiltersRoutesService";

export default function FiltersRoutes() {

    const [isShowModal, setisShowModal] = useState(false);
    let [searchParams, setSearchParams] = useSearchParams();
    const [NameForm, setNameForm] = useState(searchParams.get("name")|| "");
    const [GradeForm, setGradeForm] = useState(searchParams.get("grade")|| "");
    const [LineForm, setLineForm] = useState(searchParams.get("line")|| "");
    const [ListGrades, setListGrades] = useState([])
    const [ListLines, setListLines] = useState([])

    const clearData = () => {
        setNameForm("")
        setGradeForm("")
        setLineForm("")
    }

    const handleClose= () => {
        setisShowModal(false)
        // clearData()
    };
    const handleClearFilters= () => {
      setisShowModal(false)
      clearData()
      setSearchParams({});
    };
    const handleShow = () => {
      if (searchParams.get("name")) setNameForm(searchParams.get("name"))
      else setNameForm("");
      if (searchParams.get("grade")) setGradeForm(searchParams.get("grade"))
      else setGradeForm("");
      if (searchParams.get("line")) setLineForm(searchParams.get("line"))
      else setLineForm("");
      setisShowModal(true);
    };

    const handleFilter = (event) => {
      event.preventDefault();
      setisShowModal(false);
      let params = {}
      if (NameForm) params["name"]= NameForm;
      if (GradeForm) params["grade"]= GradeForm;
      if (LineForm) params["line"]= LineForm;
      setSearchParams(params);
    }


    useEffect(() => {
      DataService.getRoutesFilters()
        .then(
          response => {
            setListGrades(response.data.grades)
            setListLines(response.data.lines)
            }
        )
        .catch((error) => {
          // console.log("error")
        });
    }, []);

    return (
        <>
        <Button size="sm" onClick={handleShow}>Filters</Button>
        <Modal show={isShowModal} onHide={handleClose} centered>
              <Modal.Header>
              <Modal.Title>Filters</Modal.Title>
              </Modal.Header>
              <Modal.Body>
              <Form>
                  <Form.Group className="mb-3">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      type="text" rows={4}
                      onChange={(event) => setNameForm(event.target.value)}
                      value={NameForm}
                      
                    />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Grade</Form.Label>
                <Form.Select aria-label="Grade"
                onChange={(event) => setGradeForm(event.target.value)}
                value={GradeForm}>
                  <option key="default"></option>
                  {ListGrades && ListGrades.map((key) => {
                    return <option key={key} value={key}>{key}</option>;
                  })}
                </Form.Select>
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Line</Form.Label>
                <Form.Select aria-label="Line"
                onChange={(event) => setLineForm(event.target.value)}
                value={LineForm}>
                  <option key="default"></option>
                  {ListLines && ListLines.map((key) => {
                    return <option key={key} value={key}>{key}</option>;
                  })}
                </Form.Select>
              </Form.Group>
                </Form>
              </Modal.Body>
              <Modal.Footer  >
              <Button
                  onClick={handleClearFilters} variant="dark"
                >Clear filters</Button>
              <Button
                  onClick={handleClose}
                >Cancel</Button>
              <Button
                  onClick={handleFilter} variant="success"
                >Apply</Button>
              </Modal.Footer>
            </Modal>
            </>
    )
}



 
 
 

// export default function СonfirmationModal({uuidCouvertureCodenoq, tableRefresh }:IDescriptionModalProp) {
 
// const [show, setShow] = useState(false);
 

// const handleClose= () => {
//        setShow(false)
// };
 

//   const handleShow = () => {
  
//     setShow(true);
// };

// const handleDelete = () => {
 
//   DataService.deleteCouvertureMetier(uuidCouvertureCodenoq)
//   .then(() => {
//     setShow(false);
//     tableRefresh(Date.now())
//   })

//   .catch((error) => {
//      //error
//   });
// };




//   return (
//     <div>
//  <SuperButton
//         icon="trash"
//         size="sqr"
//         variant="secondary"
//         onClick={handleShow}
//       />
//          <Modal show={show} onHide={handleClose}>
//               <Modal.Header closeButton  >
//                 <h6>{TextList.header_modal}</h6>
//               </Modal.Header>
//               <Modal.Body>
//                {TextList.discription_modal}
//               </Modal.Body>
//               <Modal.Footer  >
//               <SuperButton
//                   variant="outline-primary"
//                   size="sm"
//                   label="Annuler"
//                   onClick={handleClose}
//                 />
//               <SuperButton
//                   variant="danger"
//                   size="sm"
//                   label="Supprimer"
//                   onClick={handleDelete}
//                 />
 
//               </Modal.Footer>
//             </Modal>
 
//     </div>
//   );
// }