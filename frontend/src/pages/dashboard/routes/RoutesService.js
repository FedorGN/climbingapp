import { axiosInstance } from "../../../assets/utils/axios/Axios"
import myAppConfig from "../../../config";

const getRoutes = async (name, grade, line, uuid_user, page, size) => {
  try {
    const response = await axiosInstance.get(
      myAppConfig.api.ENDPOINT + "/api/v1/routes/get-routes",
      {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
        params: {
          "name": name,
          "grade": grade,
          "line": line,
          "uuid_user": uuid_user,
          "page": page,
          "size": size
        }
      })
    return response;
  } catch (error) {
    throw new Error(`Bad request`);
  };
}


const postUpdateUserRoute = (send) => {
  return axiosInstance
    .post(myAppConfig.api.ENDPOINT + "/api/v1/routes/user-route-update", send, {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      }
    })
    .then((response) => {
      return response;
    });
};


const DataService = {
  getRoutes,
  postUpdateUserRoute
};

export default DataService;