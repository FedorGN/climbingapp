import { useState, useEffect } from "react"
import { useParams } from "react-router-dom"
import BootstrapTable from 'react-bootstrap-table-next';
import ReactPaginate from "react-paginate"

import DataService from "./ClimberTabsService"
import {
    formatterRouteName
} from "../../../../assets/utils/formatters/TableFormatters"

export default function ClimberTops(props) {
    const [ItemsRoutes, setItemsRoutes] = useState([]);
    const [pageCount, setpageCount] = useState(0);
    const [newPage, setnewPage] = useState(1);
    const [refresh, setRefresh] = useState(0);
    const params = useParams();

    const itemsPerPage = 20;

    useEffect(() => {
        DataService.getRoutes(null, null, null, params.uuid_user, newPage, itemsPerPage)
            .then(
                response => {
                    setItemsRoutes(response.data.items)
                    setpageCount(Math.ceil(response.data.total / itemsPerPage));
                }
            )
            .catch((error) => {
                // console.log("error")
            });
    }, [refresh, newPage, params.uuid_user, props.refresh]);

    const handlePageClick = (event) => {
        setnewPage(event.selected + 1);
    };


    const columns = [
        {
            dataField: 'line_name',
            text: 'Line'
        },
        {
            dataField: 'route_name',
            text: 'Name',
            formatter: formatterRouteName,
            formatExtraData: ItemsRoutes
        }, {
            dataField: 'grade',
            text: 'Grade'
        }];


    return (
        <>
            <BootstrapTable bordered={false} hover keyField='id' data={ItemsRoutes} columns={columns} />
            <ReactPaginate
                className="pagination justify-content-center"
                nextLabel=">"
                onPageChange={handlePageClick}
                pageRangeDisplayed={3}
                marginPagesDisplayed={2}
                pageCount={pageCount}
                previousLabel="<"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakLabel="..."
                breakClassName="page-item"
                breakLinkClassName="page-link"
                containerClassName="pagination"
                activeClassName="active"
                renderOnZeroPageCount={null}
            />
        </>
    );
}
