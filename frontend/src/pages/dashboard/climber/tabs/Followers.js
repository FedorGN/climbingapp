import { useState, useEffect } from "react"
import { useParams } from "react-router-dom"
import ReactPaginate from "react-paginate"
import BootstrapTable from 'react-bootstrap-table-next'

import DataService from "./ClimberTabsService"
import {
    formatterClimberFullName
} from "../../../../assets/utils/formatters/TableFormatters"


export default function UserFollowers(props) {
    const [itemsUserFollowers, setitemsUserFollowers] = useState([]);
    const [pageCount, setpageCount] = useState(0);
    const [newPage, setnewPage] = useState(1);

    const params = useParams();

    const itemsPerPage = 20;


    useEffect(() => {
        DataService.getUserFollower(props.type, params.uuid_user, newPage, itemsPerPage)
            .then(
                response => {
                    setitemsUserFollowers(response.data.items)
                    setpageCount(Math.ceil(response.data.total / itemsPerPage));
                }
            )
            .catch((error) => {
                // console.log("error")
            });
    }, [newPage, params.uuid_user, props.refresh]);
    // TODO params.uuid_user add useless API request when changing a user by clicking on Info
    // fix it

    const handlePageClick = (event) => {
        setnewPage(event.selected + 1);
    };


    const columns = [
        {
            dataField: 'full_name',
            text: 'Name',
            formatter: formatterClimberFullName,
            formatExtraData: itemsUserFollowers
        }];


    return (
        <>
            <BootstrapTable bordered={false} hover keyField='id' data={itemsUserFollowers} columns={columns} />
            <ReactPaginate
                className="pagination justify-content-center"
                nextLabel=">"
                onPageChange={handlePageClick}
                pageRangeDisplayed={3}
                marginPagesDisplayed={2}
                pageCount={pageCount}
                previousLabel="<"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakLabel="..."
                breakClassName="page-item"
                breakLinkClassName="page-link"
                containerClassName="pagination"
                activeClassName="active"
                renderOnZeroPageCount={null}
            />
        </>
    );
}
