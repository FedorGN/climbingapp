import myAppConfig from "../../../../config";
import { axiosInstance } from "../../../../assets/utils/axios/Axios"

const getRoutes = async (name, grade, line, uuid_user, page, size) => {
    try {
        const response = await axiosInstance.get(
            myAppConfig.api.ENDPOINT + "/api/v1/routes/get-routes",
            {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem("token"),
                },
                params: {
                    "name": name,
                    "grade": grade,
                    "line": line,
                    "uuid_user": uuid_user,
                    "page": page,
                    "size": size
                }
            })
        return response;
    } catch (error) {
        throw new Error(`Bad request`);
    };
}


const getUserFollower = async (type, uuid_user, page, size) => {
    try {
        const response = await axiosInstance.get(
            myAppConfig.api.ENDPOINT + "/api/v1/users/get-followers",
            {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem("token"),
                },
                params: {
                    "type": type,
                    "uuid_user": uuid_user,
                    "page": page,
                    "size": size
                }
            })
        return response;
    } catch (error) {
        throw new Error(`Bad request`);
    };
}


const DataService = {
    getRoutes,
    getUserFollower
};

export default DataService;