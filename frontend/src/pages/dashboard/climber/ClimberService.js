import myAppConfig from "../../../config";
import { axiosInstance } from "../../../assets/utils/axios/Axios"


const getClimberInfo = async (uuid_user) => {
  try {
    const response = await axiosInstance.get(
      myAppConfig.api.ENDPOINT + "/api/v1/users/get-user-info",
      {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
        params: {
          "uuid_user": uuid_user
        }
      })
    return response;
  } catch (error) {
    throw new Error(`Bad request`);
  };
}


const postFollow = async (uuid_user) => {
  return axiosInstance.post(
    myAppConfig.api.ENDPOINT + "/api/v1/users/follow-user", {},
    {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      params: {
        "uuid_user": uuid_user
      }
    }).then((response) => {
      return response;
    });
}


const deleteFollow = async (uuid_user) => {
  return axiosInstance.delete(
    myAppConfig.api.ENDPOINT + "/api/v1/users/follow-user",
    {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      params: {
        "uuid_user": uuid_user
      }
    }).then((response) => {
      return response;
    });
}

const DataService = {
  getClimberInfo,
  deleteFollow,
  postFollow
};

export default DataService;