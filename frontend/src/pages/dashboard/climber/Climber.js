import { useState, useEffect } from "react"
import { useSearchParams, useParams } from "react-router-dom";
import { format, parseISO } from 'date-fns'
import {
  Button, Container, Row, Col,
  Card, Tab, Tabs, Stack
} from "react-bootstrap";

import DataService from "./ClimberService"
import ClimberTops from "./tabs/Tops"
import UserFollowers from "./tabs/Followers"
import LazyLoad from "../../../assets/utils/lazy_load/LazyLoad"

export default function ClimberInfo(props) {
  const [Name, setName] = useState("");
  const [MaxGrade, setMaxGrade] = useState("");
  const [About, setAbout] = useState("");
  const [LastConnectionDate, setLastConnectionDate] = useState(null);
  const [CreatedDate, setCreatedDate] = useState(null);
  const [CountTops, setCountTops] = useState("");
  const [isMe, setisMe] = useState(true);
  const [userUUID, setuserUUID] = useState("");
  const [isFollowed, setisFollowed] = useState(false);
  const [isSendingRequest, setisSendingRequest] = useState(false);
  const [searchParams, setSearchParams] = useSearchParams();
  const [tabKey, setTabKey] = useState(searchParams.get("tab") || "info");
  const [tabTopRefresh, setTabTopRefresh] = useState(0);
  const [tabFollowersRefresh, setTabFollowersRefresh] = useState(0);
  const [tabFollowingRefresh, setTabFollowingRefresh] = useState(0);
  const [isLoading, setisLoading] = useState(true);

  const params = useParams();

  function refreshTabTops(key) {
    if (key === "tops") {
      setTabTopRefresh(Date.now())
    }
  }

  function refreshTabFollowers(key) {
    if (key === "followers") {
      setTabFollowersRefresh(Date.now())
    }
  }

  function refreshTabFollowing(key) {
    if (key === "following") {
      setTabFollowingRefresh(Date.now())
    }
  }

  useEffect(() => {
    setisLoading(true)
    DataService.getClimberInfo(params.uuid_user)
      .then(
        response => {
          setName(response.data.full_name)
          setMaxGrade(response.data.climbing_grade)
          setAbout(response.data.about)
          setLastConnectionDate(response.data.date_last_login)
          setCreatedDate(response.data.date_creation)
          setCountTops(response.data.count_tops)
          setisMe(response.data.is_me)
          setisFollowed(response.data.is_followed)
          setuserUUID(response.data.uuid)

          setisLoading(false)
        }
      )
      .catch((error) => {
        // console.log("error")
      });
  }, [params.uuid_user]);


  const handleFollow = (event) => {
    setisSendingRequest(true)
    DataService.postFollow(userUUID)
      .then(
        () => {
          setisFollowed(true)
          setisSendingRequest(false)
          refreshTabFollowers(tabKey)
        }
      )
      .catch((error) => {
        setisSendingRequest(false)
        // console.log("error")
      });
  }

  const handleUnfollow = (event) => {
    setisSendingRequest(true)
    DataService.deleteFollow(userUUID)
      .then(
        response => {
          setisFollowed(false)
          setisSendingRequest(false)
          refreshTabFollowers(tabKey)
        }
      )
      .catch((error) => {
        setisSendingRequest(false)
      });
  }

  const handleTabClick = (k) => {
    setSearchParams({ "tab": k })
  };

  useEffect(() => {
    if (searchParams.get("tab")) {
      setTabKey(searchParams.get("tab"))
    }
    else {
      setTabKey("info")
      setSearchParams({ "tab": "info" }, { replace: true })
    }
  }, [params.uuid_user, searchParams]);

  let LastConnectionDateStr = ""
  let CreatedDateStr = ""
  if (LastConnectionDate) {
    LastConnectionDateStr = format(parseISO(LastConnectionDate), 'dd/MM/yyyy');
  }
  if (CreatedDate) {
    CreatedDateStr = format(parseISO(CreatedDate), 'dd/MM/yyyy');
  }

  if (isLoading) { return (<></>) }

  return (
    <>
      <Container>
        <Row className="justify-content-center pt-5 ">
          <Col xs={12} sm={10} md={8} lg={6} xl={6} >
            <Card>
              <Card.Body>
                <Card.Title className="lh-lg">
                  <Stack direction="horizontal" gap={3}>
                    <div >Climber {Name}</div>
                    <div className={isMe ? "d-none" : "ms-auto"}>
                      <Button size="sm" onClick={isFollowed ? handleUnfollow : handleFollow}
                        disabled={isSendingRequest} variant={isFollowed ? "secondary" : "success"}>
                        {isFollowed ? "Unfollow" : "Follow"}
                      </Button>
                    </div>
                  </Stack>
                </Card.Title>
                <Tabs
                  id="controlled-tab-example"
                  activeKey={tabKey}
                  onSelect={(k) => handleTabClick(k)}
                  className="mb-3"
                >
                  <Tab eventKey="info" title="Info">
                    <p><span className="fw-bold">About:</span> {About}</p>
                    <p><span className="fw-bold">Max grade:</span> {MaxGrade}</p>
                    <p><span className="fw-bold">Last connected:</span> {LastConnectionDateStr}</p>
                    <p><span className="fw-bold">Created:</span> {CreatedDateStr}</p>
                    <p><span className="fw-bold">Tops:</span> {CountTops}</p>
                  </Tab>
                  <Tab eventKey="tops" title="Tops">
                    <LazyLoad visible={tabKey === "tops"}>
                      <ClimberTops refresh={tabTopRefresh} />
                    </LazyLoad>

                  </Tab>
                  <Tab eventKey="followers" title="Followers">
                    <LazyLoad visible={tabKey === "followers"}>
                      <UserFollowers type="followers" refresh={tabFollowersRefresh} />
                    </LazyLoad>
                  </Tab>
                  <Tab eventKey="following" title="Following">
                    <LazyLoad visible={tabKey === "following"}>
                      <UserFollowers type="following" refresh={tabFollowingRefresh} />
                    </LazyLoad>
                  </Tab>
                </Tabs>

              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
