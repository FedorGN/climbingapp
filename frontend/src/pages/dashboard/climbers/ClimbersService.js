import myAppConfig from "../../../config";
import { axiosInstance } from "../../../assets/utils/axios/Axios"

const getClimbers = async (first_name, last_name, page, size) => {
  try {
    const response = await axiosInstance.get(
      myAppConfig.api.ENDPOINT + "/api/v1/users/get-climbers",
      {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
        params: {
          "first_name": first_name,
          "last_name": last_name,
          "page": page,
          "size": size
        }
      })
    return response;
  } catch (error) {
    throw new Error(`Bad request`);
  };
}


const DataService = {
  getClimbers
};

export default DataService;