import myAppConfig from "../../../../config";
import { axiosInstance } from "../../../../assets/utils/axios/Axios"

const getRoutesFilters = async () => {
  try {
    const response = await axiosInstance.get(
      myAppConfig.api.ENDPOINT + "/api/v1/routes/get-routes-filters",
      {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        }
      })
    return response;
  } catch (error) {
    throw new Error(`Bad request`);
  };
}


const DataService = {
  getRoutesFilters
};

export default DataService;