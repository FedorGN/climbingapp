import { useState } from "react";
import { useSearchParams } from "react-router-dom";
import { Button, Modal, Form } from "react-bootstrap";

export default function FiltersClimbers() {

  const [isShowModal, setisShowModal] = useState(false);
  let [searchParams, setSearchParams] = useSearchParams();
  const [FirstNameForm, setFirstNameForm] = useState(searchParams.get("first_name") || "");
  const [LastNameForm, setLastNameForm] = useState(searchParams.get("last_name") || "");
  const [GradeForm, setGradeForm] = useState(searchParams.get("grade") || "");

  const clearData = () => {
    setFirstNameForm("")
    setLastNameForm("")
    setGradeForm("")
  }

  const handleClose = () => {
    setisShowModal(false)
    // clearData()
  };
  const handleClearFilters = () => {
    setisShowModal(false)
    clearData()
    setSearchParams({});
  };
  const handleShow = () => {
    if (searchParams.get("first_name")) setFirstNameForm(searchParams.get("first_name"))
    else setFirstNameForm("");
    if (searchParams.get("last_name")) setLastNameForm(searchParams.get("last_name"))
    else setLastNameForm("");
    setisShowModal(true);
  };

  const handleFilter = (event) => {
    event.preventDefault();
    setisShowModal(false);
    // clearData()
    let params = {}
    if (FirstNameForm) params["first_name"] = FirstNameForm;
    if (LastNameForm) params["last_name"] = LastNameForm;
    if (GradeForm) params["grade"] = GradeForm;
    setSearchParams(params);
  }


  return (
    <>
      <Button size="sm" onClick={handleShow}>Filters</Button>
      <Modal show={isShowModal} onHide={handleClose} centered>
        <Modal.Header>
          <Modal.Title>Filters</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>First name</Form.Label>
              <Form.Control
                type="text" rows={4}
                onChange={(event) => setFirstNameForm(event.target.value)}
                value={FirstNameForm}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Last name</Form.Label>
              <Form.Control
                type="text" rows={4}
                onChange={(event) => setLastNameForm(event.target.value)}
                value={LastNameForm}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer  >
          <Button
            onClick={handleClearFilters} variant="dark"
          >Clear filters</Button>
          <Button
            onClick={handleClose}
          >Cancel</Button>
          <Button
            onClick={handleFilter} variant="success"
          >Apply</Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}
