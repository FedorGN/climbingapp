import { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { Button, Container, Row, Col, Card } from "react-bootstrap";
import BootstrapTable from 'react-bootstrap-table-next';
import ReactPaginate from "react-paginate"

import DataService from "./ClimbersService";
import FiltersClimbers from "./filters_climbers/FiltersClimbers"
import {
  formatterClimberFullName
} from "../../../assets/utils/formatters/TableFormatters"


export default function ClimbersList() {

  let [searchParams, setSearchParams] = useSearchParams();
  const [ItemsClimbers, setItemsClimbers] = useState([]);
  const [pageCount, setpageCount] = useState(0);
  const [newPage, setnewPage] = useState(searchParams.get("page") || 1);
  const [isLoading, setisLoading] = useState(true);

  const itemsPerPage = 20;

  const handleClearFilters = () => {
    setSearchParams({});
    setnewPage(1)
  };


  const columns = [{
    dataField: 'user_full_name',
    text: 'Name',
    formatter: formatterClimberFullName,
    formatExtraData: ItemsClimbers
  }, {
    dataField: 'count_tops',
    text: 'Tops'
  }, {
    dataField: 'climbing_grade',
    text: 'Max grade'
  }];

  useEffect(() => {
    setnewPage(searchParams.get("page") || 1)
    setisLoading(true)
    DataService.getClimbers(searchParams.get("first_name"), searchParams.get("last_name"), searchParams.get("page"), itemsPerPage)
      .then(
        response => {
          setItemsClimbers(response.data.items)
          setpageCount(Math.ceil(response.data.total / itemsPerPage));
          setisLoading(false)
        }
      )
      .catch((error) => {
        // console.log("error")
      });
  }, [searchParams]);

  const handlePageClick = (event) => {
    let updatedSearchParams = new URLSearchParams(searchParams.toString());
    updatedSearchParams.set('page', event.selected + 1);
    setSearchParams(updatedSearchParams.toString());
    setnewPage(event.selected + 1);
  };

  if (isLoading) { return (<></>) }

  return (
    <>
      <Container>
        <Row className="justify-content-center pt-5 ">
          <Col xs={12}>
            <Card>
              <Card.Body>
                <Card.Title>
                  <Container fluid className="p-0">
                    <Row>
                      <Col xs={6}>Climbers</Col>
                      <Col xs={6} className="d-flex justify-content-end gap-2">
                        <Button
                          size="sm"
                          onClick={handleClearFilters}
                          variant="dark"
                          disabled={
                            !(searchParams.has("first_name") || searchParams.has("last_name"))
                          }>
                          Clear filters
                        </Button>
                        <FiltersClimbers />
                      </Col>
                    </Row>
                  </Container></Card.Title>
                <BootstrapTable bordered={false} hover keyField='id' data={ItemsClimbers} columns={columns} />
                <ReactPaginate
                  className="pagination justify-content-center"
                  nextLabel=">"
                  onPageChange={handlePageClick}
                  forcePage={newPage ? newPage - 1 : 0}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={pageCount}
                  previousLabel="<"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
