import { axiosInstance } from "../../../assets/utils/axios/Axios"
import myAppConfig from "../../../config";

const getNews = async (page, size) => {
  try {
    const response = await axiosInstance.get(
      myAppConfig.api.ENDPOINT + "/api/v1/events/get-events",
      {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
        params: {
          "page": page,
          "size": size
        }
      })
    return response;
  } catch (error) {
    throw new Error(`Bad request`);
  };
}


const DataService = {
  getNews
};

export default DataService;