import { useState, useEffect } from "react";
import { useSearchParams, Link } from "react-router-dom";
import { Container, Row, Col, Card } from "react-bootstrap";
import BootstrapTable from 'react-bootstrap-table-next';
import ReactPaginate from "react-paginate"
import { format, parseISO } from 'date-fns'

import DataService from "./NewsService";
import { RouterPath } from "../../../assets/dictionary/RouterPath";
import { EventType } from "./Constants";

export default function NewsList() {

  let [searchParams, setSearchParams] = useSearchParams();
  const [refresh, setRefresh] = useState(0);
  const [itemsNews, setItemsNews] = useState([]);
  const [pageCount, setpageCount] = useState(0);
  const [newPage, setnewPage] = useState(1);
  const [isLoading, setisLoading] = useState(true);

  const itemsPerPage = 20;


  function FormatterNewsItem(
    cell,
    row,
    rowIndex,
    formatExtraData
  ) {
    if (formatExtraData[rowIndex].event_type === EventType.USER_FOLLOWED_USER) {
      return <>
        &#127806; <Link className="text-decoration-none" to={RouterPath.LIST_CLIMBERS + "/" + formatExtraData[rowIndex].uuid_user}>
          {formatExtraData[rowIndex].user_full_name}
        </Link> followed <Link className="text-decoration-none" to={RouterPath.LIST_CLIMBERS + "/" + formatExtraData[rowIndex].uuid_followed_user}>
          {formatExtraData[rowIndex].followed_user_full_name}
        </Link>.
      </>
    }
    else if (formatExtraData[rowIndex].event_type === EventType.USER_SENT_ROUTE) {
      return <>
        &#129495; <Link className="text-decoration-none" to={RouterPath.LIST_CLIMBERS + "/" + formatExtraData[rowIndex].uuid_user}>
          {formatExtraData[rowIndex].user_full_name}
        </Link> sent the route {formatExtraData[rowIndex].grade_route} <Link className="text-decoration-none" to={RouterPath.LIST_ROUTES + "/" + formatExtraData[rowIndex].uuid_route}>
          {formatExtraData[rowIndex].route_name}
        </Link>.
      </>
    }
    else {
      return ""
    }
  }


  function FormatterDate(
    cell,
    row,
    rowIndex,
    formatExtraData
  ) {
    return format(parseISO(formatExtraData[rowIndex].date_creation), 'dd/MM/yyyy')
  }

  const columns = [{
    dataField: 'uuid_event',
    text: 'News',
    formatter: FormatterNewsItem,
    formatExtraData: itemsNews,
  }, {
    dataField: '',
    text: 'Date',
    formatter: FormatterDate,
    formatExtraData: itemsNews,
    align: 'right',
    headerAlign: 'right',
  }];

  useEffect(() => {
    DataService.getNews(newPage, itemsPerPage)
      .then(
        response => {
          setItemsNews(response.data.items)
          setpageCount(Math.ceil(response.data.total / itemsPerPage));
          setisLoading(false)
        }
      )
      .catch((error) => {
        // console.log("error")
      });
  }, [refresh, newPage, searchParams]);

  const handlePageClick = (event) => {
    setnewPage(event.selected + 1);
  };

  if (isLoading) { return (<></>) }

  return (
    <>
      <Container>
        <Row className="justify-content-center pt-5 ">
          <Col xs={12}>
            <Card>
              <Card.Body>
                <Card.Title>
                  <Container fluid className="p-0">
                    <Row>
                      <Col>News</Col>
                    </Row>
                  </Container></Card.Title>
                <BootstrapTable bordered={false} hover keyField='id' data={itemsNews} columns={columns} />
                <ReactPaginate
                  className="pagination justify-content-center"
                  nextLabel=">"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={pageCount}
                  previousLabel="<"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
