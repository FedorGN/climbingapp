export const EventType = {
    USER_SENT_ROUTE: "eUserSentRoute",
    USER_FOLLOWED_USER: "eUserFollowedUser"
};
Object.freeze(EventType);
