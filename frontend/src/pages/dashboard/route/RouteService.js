import { axiosInstance } from "../../../assets/utils/axios/Axios"
import myAppConfig from "../../../config";

const getRouteInfo = async (uuid_route) => {
  try {
    const response = await axiosInstance.get(
      myAppConfig.api.ENDPOINT + "/api/v1/routes/get-route-info",
      {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
        params: {
          "uuid_route": uuid_route
        }
      })
    return response;
  } catch (error) {
    throw new Error(`Bad request`);
  };
}


const postUpdateUserRoute = (send) => {
  return axiosInstance
    .post(myAppConfig.api.ENDPOINT + "/api/v1/routes/user-route-update", send, {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      }
    })
    .then((response) => {
      return response;
    });
};


const DataService = {
  getRouteInfo,
  postUpdateUserRoute
};

export default DataService;