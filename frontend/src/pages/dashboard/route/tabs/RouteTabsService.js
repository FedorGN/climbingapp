import myAppConfig from "../../../../config";
import { axiosInstance } from "../../../../assets/utils/axios/Axios"

const getRouteTops = async (uuid_route, page, size) => {
    return await axiosInstance.get(
        myAppConfig.api.ENDPOINT + "/api/v1/routes/get-route-tops",
        {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("token"),
            },
            params: {
                "uuid_route": uuid_route,
                "page": page,
                "size": size
            }
        })
}


const DataService = {
    getRouteTops
};

export default DataService;