import { useState, useEffect } from "react"
import { useParams } from "react-router-dom"
import BootstrapTable from 'react-bootstrap-table-next';
import ReactPaginate from "react-paginate"
import { format, parseISO } from 'date-fns'

import DataService from "./RouteTabsService"
import {
    formatterClimberFullName
} from "../../../../assets/utils/formatters/TableFormatters"

export default function RouteTops(props) {
    const [ItemsClimbers, setItemsClimbers] = useState([]);
    const [pageCount, setpageCount] = useState(0);
    const [newPage, setnewPage] = useState(1);
    const [refresh, setRefresh] = useState(0);
    const params = useParams();

    const itemsPerPage = 20;

    useEffect(() => {
        DataService.getRouteTops(params.uuid_route, newPage, itemsPerPage)
            .then(
                response => {
                    setItemsClimbers(response.data.items)
                    setpageCount(Math.ceil(response.data.total / itemsPerPage));
                }
            )
            .catch((error) => {
                // console.log("error")
            });
    }, [refresh, newPage, params.uuid_route, props.refresh]);

    const handlePageClick = (event) => {
        setnewPage(event.selected + 1);
    };

    function FormatterDate(
        cell,
        row,
        rowIndex,
        formatExtraData
    ) {
        let dateStr = ""
        if (formatExtraData[rowIndex].date_done) {
            dateStr = format(parseISO(formatExtraData[rowIndex].date_done), 'dd/MM/yyyy');
        }
        return dateStr
    }

    const columns = [
        {
            dataField: 'user_full_name',
            text: 'Name',
            formatter: formatterClimberFullName,
            formatExtraData: ItemsClimbers
        },
        {
            dataField: 'date_done',
            text: 'Date',
            formatter: FormatterDate,
            formatExtraData: ItemsClimbers,
        }];


    return (
        <>
            <BootstrapTable bordered={false} hover keyField='id' data={ItemsClimbers} columns={columns} />
            <ReactPaginate
                className="pagination justify-content-center"
                nextLabel=">"
                onPageChange={handlePageClick}
                pageRangeDisplayed={3}
                marginPagesDisplayed={2}
                pageCount={pageCount}
                previousLabel="<"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakLabel="..."
                breakClassName="page-item"
                breakLinkClassName="page-link"
                containerClassName="pagination"
                activeClassName="active"
                renderOnZeroPageCount={null}
            />
        </>
    );
}
