import { useState, useEffect } from "react"
import { useParams, useSearchParams, Link } from "react-router-dom";
import { Button, Container, Row, Col, Card, Tabs, Tab, Stack } from "react-bootstrap";
import { format, parseISO } from 'date-fns'

import { RouterPath } from "../../../assets/dictionary/RouterPath";
import RouteTops from "./tabs/RouteTops"
import DataService from "./RouteService"
import LazyLoad from "../../../assets/utils/lazy_load/LazyLoad"

export default function RouteInfo(props) {
  const [RouteName, setRouteName] = useState("");
  const [Description, setDescription] = useState("");
  const [DateStart, setDateStart] = useState(null);
  const [DateEnd, setDateEnd] = useState(null);
  const [Grade, setGrade] = useState("");
  const [LineName, setLineName] = useState("");
  const [SetterName, setSetterName] = useState("");
  const [UUIDUser, setUUIDUser] = useState("");
  const [CountDone, setCountDone] = useState("");
  const [Users, setUsers] = useState([]);
  const params = useParams();
  const [isSendingRequest, setisSendingRequest] = useState(false);
  const [isDoneByMe, setisDoneByMe] = useState(false);
  const [searchParams, setSearchParams] = useSearchParams();
  const [tabKey, setTabKey] = useState(searchParams.get("tab") || "info");
  const [isLoading, setisLoading] = useState(true);
  const [tabTopRefresh, setTabTopRefresh] = useState(0);

  function refreshTabTops(key) {
    if (key === "tops") {
      setTabTopRefresh(Date.now())
    }
  }

  useEffect(() => {
    setisLoading(true)
    DataService.getRouteInfo(params.uuid_route)
      .then(
        response => {
          setRouteName(response.data.route_name)
          setDescription(response.data.description)
          setDateStart(response.data.date_start)
          setDateEnd(response.data.date_end)
          setGrade(response.data.grade)
          setLineName(response.data.line_name)
          setSetterName(response.data.setter_name)
          setUUIDUser(response.data.uuid_user)
          setCountDone(response.data.count_done)
          setisDoneByMe(response.data.is_done_by_me)
          setUsers(response.data.users)
          setisLoading(false)
        }
      )
      .catch((error) => {
        // console.log("error")
      });
  }, [params.uuid_route]);

  const handleClick = (e) => {
    setisSendingRequest(true);
    e.preventDefault();
    var data = {
      "uuid_route": params.uuid_route,
      "is_done": !isDoneByMe
    };
    DataService.postUpdateUserRoute(data)
      .then((response) => {
        refreshTabTops(tabKey)
        if (response.status === 200) {
          setisDoneByMe(false)
          setCountDone(CountDone - 1)
          setisSendingRequest(false);
        }
        else if (response.status === 201) {
          setisDoneByMe(true)
          setCountDone(CountDone + 1)
          setisSendingRequest(false);
        }
      })
      .catch((error) => {
        setisSendingRequest(false);
      });
  };

  const handleTabClick = (k) => {
    setSearchParams({ "tab": k })
    refreshTabTops(k)
  };

  useEffect(() => {
    if (searchParams.get("tab")) {
      setTabKey(searchParams.get("tab"))
    }
    else {
      setTabKey("info")
      setSearchParams({ "tab": "info" }, { replace: true })
    }
  }, [params.uuid_route, searchParams]);


  let DateStartStr = ""
  let DateEndStr = ""
  if (DateStart) {
    DateStartStr = format(parseISO(DateStart), 'dd/MM/yyyy');
  }
  if (DateEnd) {
    DateEndStr = format(parseISO(DateEnd), 'dd/MM/yyyy');
  }

  if (isLoading) { return (<></>) }

  return (
    <>
      <Container>
        <Row className="justify-content-center pt-5 ">
          <Col xs={12} sm={10} md={8} lg={6} xl={6} >
            <Card>
              <Card.Body>
                <Card.Title className="lh-lg">
                  <Stack direction="horizontal" gap={3}>
                    Route "{RouteName}"
                    <div className="ms-auto">
                      <Button size="sm" variant={isDoneByMe ? "secondary" : "success"}
                        type="submit"
                        onClick={(e) => handleClick(e)}
                        disabled={isSendingRequest}>{isDoneByMe ? "I didn't send it" : "I sent it!"}</Button>
                    </div>
                  </Stack>
                </Card.Title>
                <Tabs
                  id="controlled-tab-example"
                  activeKey={tabKey}
                  onSelect={(k) => handleTabClick(k)}
                  className="mb-3"
                >
                  <Tab eventKey="info" title="Info">
                    <p><span className="fw-bold">Grade:</span> {Grade}</p>
                    <p><span className="fw-bold">Description:</span> {Description}</p>
                    <p><span className="fw-bold">Line:</span> {LineName}</p>
                    <p><span className="fw-bold">Setter:</span> <Link className="text-decoration-none" to={RouterPath.LIST_CLIMBERS + "/" + UUIDUser}>
                      {SetterName}
                    </Link></p>
                    <p><span className="fw-bold">Dates:</span> {DateStartStr} - {DateEndStr}</p>
                    <p><span className="fw-bold">Tops:</span> {CountDone} {Users.length ? "(" : ""}
                      {
                        Users.map((user, index) => {
                          return <span key={index}>
                            {(index ? ', ' : '')}<Link className="text-decoration-none" to={RouterPath.LIST_CLIMBERS + "/" + user.uuid_user}>{user.full_name}</Link>
                          </span>
                        })}
                      {Users.length ? ")" : ""}</p>
                  </Tab>
                  <Tab eventKey="tops" title="Tops">
                    <LazyLoad visible={tabKey === "tops"}>
                      <RouteTops refresh={tabTopRefresh} />
                    </LazyLoad>
                  </Tab>
                </Tabs>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
