import { useState, useEffect } from "react"
import { Container, Row, Col, Card, Form } from "react-bootstrap"

import DataService from "./ProfilService"
import ModalModifyInfo from "./modify_info/ModalModifyInfo"
import ModalModifyPwd from "./modify_password/ModalModifyPwd"
import ModalDeleteAccount from "./delete_account/ModalDeleteAccount"

export default function Profil(props) {
  const [FirstName, setFirstName] = useState("");
  const [LastName, setLastName] = useState("");
  const [Email, setEmail] = useState("");
  const [About, setAbout] = useState("");
  const [refresh, setRefresh] = useState(0);


  useEffect(() => {
    DataService.getMyInfo()
      .then(
        response => {
          setFirstName(response.data.first_name)
          setLastName(response.data.last_name)
          setEmail(response.data.email)
          setAbout(response.data.about)
        }
      )
      .catch((error) => {
        // console.log("error")
      });
  }, [refresh]);

  return (
    <>
      <Container>
        <Row className="justify-content-center pt-5 ">
          <Col xs={12} sm={10} md={8} lg={6} xl={4} >
            <Card className="mb-5">
              <Card.Body>
                <Card.Title>Profil</Card.Title>
                <Form>
                  <Form.Group className="mb-3">
                    <Form.Label>First name</Form.Label>
                    <Form.Control value={FirstName} disabled />
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label>Last name</Form.Label>
                    <Form.Control value={LastName} disabled />
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label>Email</Form.Label>
                    <Form.Control value={Email} disabled />
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label>About</Form.Label>
                    <Form.Control as="textarea" rows={3} value={About} disabled />
                  </Form.Group>
                  <ModalModifyInfo infoRefresh={setRefresh} FirstName={FirstName} LastName={LastName} About={About} />
                  <Form.Group className="mb-3 mt-3">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" value="*******" disabled />
                  </Form.Group>
                  <ModalModifyPwd />
                </Form>
                <hr />
                <ModalDeleteAccount />
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
