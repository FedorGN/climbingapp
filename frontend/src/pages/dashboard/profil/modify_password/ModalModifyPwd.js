import { useState } from "react";
import { Button, Modal, Form} from "react-bootstrap";

import DataService from "./ModalModifyPwdService";

export default function ModalModifyInfo() {

    const [isShowModal, setisShowModal] = useState(false);
    const [isErrPwdNotSame, setisErrPwdNotSame] = useState(false)
    const [isErrPasswordInvalid, setisErrPasswordInvalid] = useState(false)
    const [isErrPasswordTooShort, setisErrPasswordTooShort] = useState(false)
    const [oldPwdForm, setoldPwdForm] = useState("");
    const [firstNewPwdForm, setfirstNewPwdForm] = useState("");
    const [secondNewPwdForm, setsecondNewPwdForm] = useState("");
    const [isSendingRequest, setisSendingRequest] = useState(false);

    const clearData = () => {
      setoldPwdForm("")
      setfirstNewPwdForm("")
      setsecondNewPwdForm("")
    }

    const clearErrors = () => {
      setisErrPwdNotSame(false);
      setisErrPasswordInvalid(false);
      setisErrPasswordTooShort(false);
    }

    const handleClose= () => {
        setisShowModal(false)
        clearErrors()
        clearData()
    };
    const handleShow = () => {
      clearData();  
      setisShowModal(true);
    };

    const data = {
      "old_password": oldPwdForm,
      "new_password": secondNewPwdForm
    }


    const handleAdd = () => {
      clearErrors()
      if (secondNewPwdForm !== firstNewPwdForm) {
        setisErrPwdNotSame(true)
      }
      else if (secondNewPwdForm.length < 6) {
        setisErrPasswordTooShort(true)
      }
      else {
        setisSendingRequest(true)
        DataService.updateUserPassword(data)
        .then(() => {
          setisShowModal(false);
          setisSendingRequest(false);
          clearData();
        })
      
        .catch((error) => {
          if (error.response.status === 400) {
            setisErrPasswordInvalid(true)
            setisSendingRequest(false);
          }
        });
      };
    }

    return (
        <>
        <Button onClick={handleShow}>Modify password</Button>
        <Modal show={isShowModal} onHide={handleClose} centered>
              <Modal.Header>
              <Modal.Title>Modify my information</Modal.Title>
              </Modal.Header>
              <Modal.Body>
              <Form>

                <Form.Group className="mb-3">
                  <Form.Label>
                    Old password<span className="text-danger">*</span>
                  </Form.Label>
                  <Form.Control
                    type="password"
                    onChange={(event) => setoldPwdForm(event.target.value)}
                    value={oldPwdForm}
                    isInvalid={isErrPasswordInvalid}
                  />
                  <Form.Text className={
                  "text-danger " +
                  (isErrPasswordInvalid ? "" : "d-none")
                }>
                    Invalid password.
                  </Form.Text>
                </Form.Group>

                  <Form.Group className="mb-3">
                    <Form.Label>
                      New password<span className="text-danger">*</span>
                    </Form.Label>
                    <Form.Control
                      type="password"
                      onChange={(event) => setfirstNewPwdForm(event.target.value)}
                      value={firstNewPwdForm}
                      isInvalid={isErrPwdNotSame || isErrPasswordTooShort}
                    />
                    <Form.Text className={
                    "text-danger " +
                    (isErrPasswordTooShort ? "" : "d-none")
                  }>
                      Min length required is 6 characters.
                    </Form.Text>
                    <Form.Text className={
                    "text-danger " +
                    (isErrPwdNotSame ? "" : "d-none")
                  }>
                      The new passwords must be the same.
                    </Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3">
                    <Form.Label>
                      Repeat new password<span className="text-danger">*</span>
                    </Form.Label>
                    <Form.Control
                      type="password"
                      onChange={(event) => setsecondNewPwdForm(event.target.value)}
                      value={secondNewPwdForm}
                      isInvalid={isErrPwdNotSame}
                    />
                    <Form.Text className={
                    "text-danger " +
                    (isErrPwdNotSame ? "" : "d-none")
                  }>
                      The new passwords must be the same.
                    </Form.Text>
                  </Form.Group>
                  
                </Form>
              </Modal.Body>
              <Modal.Footer  >
              <Button
                  onClick={handleClose}
                >Cancel</Button>
              <Button
                  onClick={handleAdd} disabled={isSendingRequest} variant="success"
                >Modify</Button>
 
              </Modal.Footer>
            </Modal>
            </>
    )
}



 
 
 

// export default function СonfirmationModal({uuidCouvertureCodenoq, tableRefresh }:IDescriptionModalProp) {
 
// const [show, setShow] = useState(false);
 

// const handleClose= () => {
//        setShow(false)
// };
 

//   const handleShow = () => {
  
//     setShow(true);
// };

// const handleDelete = () => {
 
//   DataService.deleteCouvertureMetier(uuidCouvertureCodenoq)
//   .then(() => {
//     setShow(false);
//     tableRefresh(Date.now())
//   })

//   .catch((error) => {
//      //error
//   });
// };




//   return (
//     <div>
//  <SuperButton
//         icon="trash"
//         size="sqr"
//         variant="secondary"
//         onClick={handleShow}
//       />
//          <Modal show={show} onHide={handleClose}>
//               <Modal.Header closeButton  >
//                 <h6>{TextList.header_modal}</h6>
//               </Modal.Header>
//               <Modal.Body>
//                {TextList.discription_modal}
//               </Modal.Body>
//               <Modal.Footer  >
//               <SuperButton
//                   variant="outline-primary"
//                   size="sm"
//                   label="Annuler"
//                   onClick={handleClose}
//                 />
//               <SuperButton
//                   variant="danger"
//                   size="sm"
//                   label="Supprimer"
//                   onClick={handleDelete}
//                 />
 
//               </Modal.Footer>
//             </Modal>
 
//     </div>
//   );
// }