import { axiosInstance } from "../../../../assets/utils/axios/Axios"
import myAppConfig from "../../../../config";

const updateUserPassword = async (data) => {
  return axiosInstance.post(
    myAppConfig.api.ENDPOINT + "/api/v1/users/update-password",
    data,
    {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    }).then((response) => {
      return response;
    });
}

const DataService = {
  updateUserPassword
};

export default DataService;