import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Modal, Form} from "react-bootstrap";

import DataService from "./ModalDeleteAccountService";
import { RouterPath } from "../../../../assets/dictionary/RouterPath";

export default function ModalDeleteAccount() {

    const [isShowModal, setisShowModal] = useState(false);
    const [isShowValidationError, setisShowValidationError] = useState(false)
    const [deleteForm, setdeleteForm] = useState("");
    let navigate = useNavigate();

    const clearData = () => {
      setdeleteForm("")
    }

    const clearErrors = () => {
      setisShowValidationError(false)
    }

    const handleClose= () => {
        setisShowModal(false)
        clearErrors()
        clearData()
    };
    const handleShow = () => {
        setisShowModal(true);
    };


    const handleDelete = () => {
      clearErrors()
      if (deleteForm.toLowerCase() !== "delete") {
        setisShowValidationError(true)
      }
    
      else {DataService.deleteAccount()
        .then(() => {
          setisShowModal(false);
          clearData()
          // localStorage.removeItem("token");
          localStorage.clear();
          navigate(RouterPath.HOME);
        })
        .catch((error) => {
           //error
        });
      };
    }

    return (
        <>
        <Button variant="dark" onClick={handleShow}>Delete my account</Button>
        <Modal show={isShowModal} onHide={handleClose} centered>
              <Modal.Header>
              <Modal.Title>Delete my account</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <p>You are going to delete your account. All data will be lost!</p>
              <Form>
                <Form.Group className="mb-3">
                  <Form.Label>
                    Write "delete"<span className="text-danger">*</span>
                  </Form.Label>
                  <Form.Control
                    onChange={(event) => setdeleteForm(event.target.value)}
                    value={deleteForm}
                    isInvalid={isShowValidationError}
                  />
                  <Form.Text className={
                  "text-danger " +
                  (isShowValidationError ? "" : "d-none")
                }>
                    Write "delete" here to confirm
                  </Form.Text>
                </Form.Group>
                </Form>
              </Modal.Body>
              <Modal.Footer  >
              <Button
                  onClick={handleClose}
                >Cancel</Button>
              <Button
                  onClick={handleDelete} variant="danger"
                >Delete</Button>
              </Modal.Footer>
            </Modal>
            </>
    )
}



 
 
 

// export default function СonfirmationModal({uuidCouvertureCodenoq, tableRefresh }:IDescriptionModalProp) {
 
// const [show, setShow] = useState(false);
 

// const handleClose= () => {
//        setShow(false)
// };
 

//   const handleShow = () => {
  
//     setShow(true);
// };

// const handleDelete = () => {
 
//   DataService.deleteCouvertureMetier(uuidCouvertureCodenoq)
//   .then(() => {
//     setShow(false);
//     tableRefresh(Date.now())
//   })

//   .catch((error) => {
//      //error
//   });
// };




//   return (
//     <div>
//  <SuperButton
//         icon="trash"
//         size="sqr"
//         variant="secondary"
//         onClick={handleShow}
//       />
//          <Modal show={show} onHide={handleClose}>
//               <Modal.Header closeButton  >
//                 <h6>{TextList.header_modal}</h6>
//               </Modal.Header>
//               <Modal.Body>
//                {TextList.discription_modal}
//               </Modal.Body>
//               <Modal.Footer  >
//               <SuperButton
//                   variant="outline-primary"
//                   size="sm"
//                   label="Annuler"
//                   onClick={handleClose}
//                 />
//               <SuperButton
//                   variant="danger"
//                   size="sm"
//                   label="Supprimer"
//                   onClick={handleDelete}
//                 />
 
//               </Modal.Footer>
//             </Modal>
 
//     </div>
//   );
// }