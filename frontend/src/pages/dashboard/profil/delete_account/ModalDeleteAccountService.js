import { axiosInstance } from "../../../../assets/utils/axios/Axios"
import myAppConfig from "../../../../config";

const deleteAccount = async () => {
  return axiosInstance.delete(
    myAppConfig.api.ENDPOINT + "/api/v1/users/delete-account",
    {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      }
    }).then((response) => {
      return response;
    });
}


const DataService = {
  deleteAccount
};

export default DataService;