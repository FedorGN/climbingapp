import { useState } from "react";
import { Button, Modal, Form} from "react-bootstrap";

import DataService from "./ModalModifyInfoService";

export default function ModalModifyInfo({infoRefresh, FirstName, LastName, About}) {

    const [isShowModal, setisShowModal] = useState(false);
    const [isShowValidationErrorEmptyFirstName, setisShowValidationErrorEmptyFirstName] = useState(false)
    const [isShowValidationErrorTooLongueFirstName, setisShowValidationErrorTooLongueFirstName] = useState(false)
    const [isShowValidationErrorEmptyLastName, setisShowValidationErrorEmptyLastName] = useState(false)
    const [isShowValidationErrorTooLongueLastName, setisShowValidationErrorTooLongueLastName] = useState(false)
    const [isShowValidationErrorAboutTooLongue, setisShowValidationErrorAboutTooLongue] = useState(false)
    const [FirstNameForm, setFirstNameForm] = useState("");
    const [LastNameForm, setLastNameForm] = useState("")
    const [AboutForm, setAboutForm] = useState("")

    const clearData = () => {
      setFirstNameForm("")
      setLastNameForm("")
      setAboutForm("")
    }

    const clearErrors = () => {
      setisShowValidationErrorEmptyFirstName(false);
      setisShowValidationErrorTooLongueFirstName(false);
      setisShowValidationErrorEmptyLastName(false);
      setisShowValidationErrorTooLongueLastName(false);
      setisShowValidationErrorAboutTooLongue(false);
    }

    const handleClose= () => {
        setisShowModal(false)
        clearErrors()
        // clearData()
    };
    const handleShow = () => {
        setisShowModal(true);
        setFirstNameForm(FirstName);
        setLastNameForm(LastName);
        setAboutForm(About);
    };

    const data = {
      "first_name": FirstNameForm,
      "last_name": LastNameForm,
      "about": AboutForm
    }


    const handleAdd = () => {
      clearErrors()
      if (FirstNameForm.length < 1) {
        setisShowValidationErrorEmptyFirstName(true)
      }
      else if (FirstNameForm.length >= 150) {
        setisShowValidationErrorTooLongueFirstName(true)
      }
      else if (LastNameForm.length < 1) {
        setisShowValidationErrorEmptyLastName(true)
      }
      else if (LastNameForm.length >= 150) {
        setisShowValidationErrorTooLongueLastName(true)
      }
      else if (AboutForm && AboutForm.length >= 300) {
        setisShowValidationErrorAboutTooLongue(true)
      }
      else {
        DataService.updateUserInfo(data)
          .then(() => {
            setisShowModal(false);
            clearData()
            infoRefresh(Date.now())
          })
          .catch((error) => {
            //error
          });
      }
    }

    return (
        <>
        <Button onClick={handleShow}>Modify information</Button>
        <Modal show={isShowModal} onHide={handleClose} centered>
              <Modal.Header>
              <Modal.Title>Modify my information</Modal.Title>
              </Modal.Header>
              <Modal.Body>
              <Form>
                <Form.Group className="mb-3">
                  <Form.Label>
                    First name<span className="text-danger">*</span>
                  </Form.Label>
                  <Form.Control
                    onChange={(event) => setFirstNameForm(event.target.value)}
                    value={FirstNameForm}
                    isInvalid={isShowValidationErrorEmptyFirstName || isShowValidationErrorTooLongueFirstName}
                  />
                  <Form.Text className={
                  "text-danger " +
                  (isShowValidationErrorEmptyFirstName ? "" : "d-none")
                }>
                    This field can't be empty.
                  </Form.Text>
                  <Form.Text className={
                  "text-danger " +
                  (isShowValidationErrorTooLongueFirstName ? "" : "d-none")
                }>
                    The maximum number of characters is 150.
                  </Form.Text>
                </Form.Group>

                  <Form.Group className="mb-3">
                    <Form.Label>
                      Last name<span className="text-danger">*</span>
                    </Form.Label>
                    <Form.Control
                      onChange={(event) => setLastNameForm(event.target.value)}
                      value={LastNameForm}
                      isInvalid={isShowValidationErrorEmptyLastName || isShowValidationErrorTooLongueLastName}
                    />
                    <Form.Text className={
                    "text-danger " +
                    (isShowValidationErrorEmptyLastName ? "" : "d-none")
                  }>
                      This field can't be empty.
                    </Form.Text>
                    <Form.Text className={
                    "text-danger " +
                    (isShowValidationErrorTooLongueLastName ? "" : "d-none")
                  }>
                      The maximum number of characters is 150.
                    </Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3">
                    <Form.Label>About</Form.Label>
                    <Form.Control
                      as="textarea" rows={4}
                      onChange={(event) => setAboutForm(event.target.value)}
                      value={AboutForm}
                      isInvalid={isShowValidationErrorAboutTooLongue}
                    />
                    <Form.Text className={
                    "text-danger " +
                    (isShowValidationErrorAboutTooLongue ? "" : "d-none")
                  }>
                      The maximum number of characters is 300.
                    </Form.Text>
                  </Form.Group>
                  
                </Form>
              </Modal.Body>
              <Modal.Footer  >
              <Button
                  onClick={handleClose}
                >Cancel</Button>
              <Button
                  onClick={handleAdd} variant="success"
                >Modify</Button>
 
              </Modal.Footer>
            </Modal>
            </>
    )
}



 
 
 

// export default function СonfirmationModal({uuidCouvertureCodenoq, tableRefresh }:IDescriptionModalProp) {
 
// const [show, setShow] = useState(false);
 

// const handleClose= () => {
//        setShow(false)
// };
 

//   const handleShow = () => {
  
//     setShow(true);
// };

// const handleDelete = () => {
 
//   DataService.deleteCouvertureMetier(uuidCouvertureCodenoq)
//   .then(() => {
//     setShow(false);
//     tableRefresh(Date.now())
//   })

//   .catch((error) => {
//      //error
//   });
// };




//   return (
//     <div>
//  <SuperButton
//         icon="trash"
//         size="sqr"
//         variant="secondary"
//         onClick={handleShow}
//       />
//          <Modal show={show} onHide={handleClose}>
//               <Modal.Header closeButton  >
//                 <h6>{TextList.header_modal}</h6>
//               </Modal.Header>
//               <Modal.Body>
//                {TextList.discription_modal}
//               </Modal.Body>
//               <Modal.Footer  >
//               <SuperButton
//                   variant="outline-primary"
//                   size="sm"
//                   label="Annuler"
//                   onClick={handleClose}
//                 />
//               <SuperButton
//                   variant="danger"
//                   size="sm"
//                   label="Supprimer"
//                   onClick={handleDelete}
//                 />
 
//               </Modal.Footer>
//             </Modal>
 
//     </div>
//   );
// }