import { axiosInstance } from "../../../../assets/utils/axios/Axios"
import myAppConfig from "../../../../config";

const updateUserInfo = async (data) => {
  try {
    const response = await axiosInstance.post(
      myAppConfig.api.ENDPOINT + "/api/v1/users/update-user-info",
      data,
      {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
      })

    //   console.log(response.data);
    return response;

  } catch (error) {
    throw new Error(`Bad request`);
  };
}

const updateUserPassword = async (data) => {
  try {
    const response = await axiosInstance.post(
      myAppConfig.api.ENDPOINT + "/api/v1/users/update-password",
      data,
      {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
      })

    //   console.log(response.data);
    return response;

  } catch (error) {
    throw new Error(`Bad request`);
  };
}

const DataService = {
  updateUserInfo,
  updateUserPassword
};

export default DataService;