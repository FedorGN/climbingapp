export const RouterPath = {
  HOME: "/",
  LOGIN: "/login",
  SIGNUP: "sign-up",
  SIGNUP_MAIL_SENT: "/sign-up/mail-sent",
  SIGNUP_CONFIRM_EMAIL: "/confirm-email",
  FORGOT_PASSWORD: "/forgot-password",
  FORGOT_PASSWORD_MAIL_SENT: "/password-forgot/mail-sent",
  RESET_PASSWORD: "/reset-password",
  PASSWORD_CHANGED: "/reset-password/password-changed",
  LINK_NOT_VALID: "/link-not-valid",
  LIST_ROUTES: "/routes",
  LIST_CLIMBERS: "/climbers",
  LIST_NEWS: "/news",
  MY_INFORMATION: "/my-profile",
  LIST_TODOS: "/list-todos",
  LIST_DONE: "/list-done",
  MAILHOG: "/mailhog",
  FLOWER: "/flower",
  ABOUT_PAGE: "/about"
};
Object.freeze(RouterPath);
