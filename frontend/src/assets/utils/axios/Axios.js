import axios from "axios";
import { RouterPath } from "../../dictionary/RouterPath";

export const axiosInstance = axios.create();

axiosInstance.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response.status === 403) {
        localStorage.clear()
        window.location.href = RouterPath.LOGIN;
    }
    return error
});