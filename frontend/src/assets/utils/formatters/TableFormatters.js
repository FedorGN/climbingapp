import { RouterPath } from "../../dictionary/RouterPath";
import { Link } from "react-router-dom";

export function formatterRouteName(
    cell,
    row,
    rowIndex,
    formatExtraData
) {
    return (
        <Link className="text-decoration-none" to={RouterPath.LIST_ROUTES + "/" + formatExtraData[rowIndex].uuid}>
            {formatExtraData[rowIndex].route_name}
        </Link>
    );
}

export function formatterRouteLine(
    cell,
    row,
    rowIndex,
    formatExtraData
) {
    return (
        <Link className="text-decoration-none" to={RouterPath.LIST_ROUTES + "?line=" + formatExtraData[rowIndex].line_name}>
            {formatExtraData[rowIndex].line_name}
        </Link>
    );
}


export function formatterRouteGrade(
    cell,
    row,
    rowIndex,
    formatExtraData
) {
    return (
        <Link className="text-decoration-none"
            to={RouterPath.LIST_ROUTES + "?grade=" + encodeURIComponent(formatExtraData[rowIndex].grade)}>
            {formatExtraData[rowIndex].grade}
        </Link>
    );
}


export function formatterClimberFullName(
    cell,
    row,
    rowIndex,
    formatExtraData
) {
    return (
        <Link className="text-decoration-none"
            to={RouterPath.LIST_CLIMBERS + "/" + formatExtraData[rowIndex].uuid_user}>
            {formatExtraData[rowIndex].user_full_name}
        </Link>
    );
}