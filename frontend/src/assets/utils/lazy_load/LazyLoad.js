import { useRef } from "react"


export default function LazyLoad(props) {
    // const rendered = useRef(props.visible);

    // if (props.visible && !rendered.current) {
    //     rendered.current = true;
    // }

    // if (!rendered.current)
    //     return null;

    if (!props.visible)
        return null;

    return <div className={props.visible ? '' : 'd-none'}>{props.children}</div>;
}