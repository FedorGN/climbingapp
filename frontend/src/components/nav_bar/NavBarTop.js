import { useEffect, useState } from "react"
import { Link, useNavigate, useLocation } from "react-router-dom";
import { Button, Container, Row, Col, Navbar, Nav } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

import { RouterPath } from "../../assets/dictionary/RouterPath";
import DataService from "./NavBarTopService"

export default function NavBarTop(props) {
  const isAuthenticated = localStorage.getItem("token") ? true : false;
  let navigate = useNavigate();
  const location = useLocation();
  const [userUUID, setuserUUID] = useState("");

  useEffect(() => {
    if (isAuthenticated) {
      DataService.getAuth()
        .then(
          response => {
            setuserUUID(response.data.uuid)
          }
        )
        .catch((error) => {
          // console.log("error")
        });
    }
    else { setuserUUID("") }
  }, [isAuthenticated]);

  const handleClickLogOut = (e) => {
    e.preventDefault();
    localStorage.removeItem("token");
    navigate(RouterPath.HOME);
  };

  return (
    <>
      <Container fluid>
        <Row>
          <Col className="p-0">
            <Navbar bg="dark" variant="dark" expand="md">
              <Container className="pe-0">
                <Row className="w-100 justify-content-between">
                  <Col xs={8} md={9} className="d-flex">
                    <LinkContainer className="brand" to={RouterPath.HOME}>
                      <Navbar.Brand>Climbing App</Navbar.Brand>
                    </LinkContainer>
                    {isAuthenticated && (
                      <>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" className="me-3" />
                        <Navbar.Collapse id="basic-navbar-nav">
                          <Nav className="me-auto" activeKey={location.pathname}>
                            <LinkContainer to={RouterPath.LIST_NEWS}>
                              <Nav.Link>News</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={RouterPath.LIST_ROUTES}>
                              <Nav.Link>Routes</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={RouterPath.LIST_CLIMBERS}>
                              <Nav.Link>Climbers</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={RouterPath.LIST_CLIMBERS + "/" + userUUID}>
                              <Nav.Link>My page</Nav.Link>
                            </LinkContainer>
                          </Nav>
                        </Navbar.Collapse>
                      </>
                    )}
                  </Col>
                  <Col
                    xs={3}
                    className="align-items-end d-flex flex-row-reverse gap-2"
                  >
                    {!isAuthenticated && (
                      <>
                        <Link to={RouterPath.SIGNUP}>
                          <Button className="text-nowrap">Sign up</Button>
                        </Link>
                        <Link className="ml-1" to={RouterPath.LOGIN}>
                          <Button variant="success">Login</Button>
                        </Link>
                      </>
                    )}
                    {isAuthenticated && (
                      <>
                        <Link className="text-nowrap" to={RouterPath.MY_INFORMATION}>
                          <Button>My account</Button>
                        </Link>
                        <Button className="text-nowrap ml-1"
                          variant="info"
                          onClick={(e) => handleClickLogOut(e)}
                        >
                          Log out
                        </Button>
                      </>
                    )}
                  </Col>
                </Row>
              </Container>
            </Navbar>
          </Col>
        </Row>
      </Container>
    </>
  );
}
