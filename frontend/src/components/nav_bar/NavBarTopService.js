import myAppConfig from "../../config";
import { axiosInstance } from "../../assets/utils/axios/Axios"

const getAuth = async () => {
  return axiosInstance.post(
    myAppConfig.api.ENDPOINT + "/api/v1/login/verify-token", {},
    {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    }).then((response) => {
      return response;
    });
};

const DataService = {
  getAuth
};

export default DataService;
