import { Outlet } from "react-router-dom";

import NavBarTop from "./components/nav_bar/NavBarTop";

function App() {
  document.title = "Climbing App | Roc14"
  document.description = "Climbing App of the parisian climbing club Roc14."
  return (
    <>
      <NavBarTop />
      <Outlet />
    </>
  );
}

export default App;
