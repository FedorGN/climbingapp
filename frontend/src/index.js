import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { render } from "react-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import 'bootstrap-icons/font/bootstrap-icons.css';

import { RouterPath } from "./assets/dictionary/RouterPath";
import { PrivateRoute } from "./components/auth/PrivateRoute";

import App from "./App";
import HomePage from "./pages/home_page/HomePage";
import AboutPage from "./pages/home_page/about_page/AboutPage";
import Login from "./pages/login/Login";
import SignUpForm from "./pages/sign_up/sign_up_form/SignUpForm";
import ConfirmationEmailSent from "./pages/sign_up/confirmation_email_sent/ConfirmationEmailSent";
import NewsList from "./pages/dashboard/news/News";
import RoutesList from "./pages/dashboard/routes/Routes";
import RouteInfo from "./pages/dashboard/route/Route"
import ClimbersList from "./pages/dashboard/climbers/Climbers"
import ClimberInfo from "./pages/dashboard/climber/Climber"
import Profil from "./pages/dashboard/profil/Profil";
import ForgotPasswordForm from "./pages/forgot_password/forgot_password_form/ForgotPasswordForm";
import ForgotPasswordEmailSent from "./pages/forgot_password/forgot_password_email_sent/ForgotPasswordEmailSent";
import ResetPasswordForm from "./pages/forgot_password/reset_password/ResetPassword";
import PasswordChanged from "./pages/forgot_password/password_changed/PasswordChanged";
import LinkNotValid from "./pages/link_not_valid/LinkNotValid";
import ConfirmEmail from "./pages/sign_up/confirm_email/ConfirmEmail";

const rootElement = document.getElementById("root");
render(
  <BrowserRouter>
    <Routes>
      <Route path={RouterPath.HOME} element={<App />}>
        <Route exact path={RouterPath.HOME} element={<HomePage />}></Route>
        <Route exact path={RouterPath.ABOUT_PAGE} element={<AboutPage />}></Route>
        <Route path={RouterPath.LOGIN} element={<Login />} />
        <Route path={RouterPath.SIGNUP} element={<SignUpForm />} />
        <Route
          path={RouterPath.SIGNUP_MAIL_SENT}
          element={<ConfirmationEmailSent />}
        />
        <Route
          path={RouterPath.SIGNUP_CONFIRM_EMAIL}
          element={<ConfirmEmail />}
        />
        <Route
          path={RouterPath.FORGOT_PASSWORD}
          element={<ForgotPasswordForm />}
        />
        <Route
          path={RouterPath.FORGOT_PASSWORD_MAIL_SENT}
          element={<ForgotPasswordEmailSent />}
        />
        <Route
          path={RouterPath.RESET_PASSWORD}
          element={<ResetPasswordForm />}
        />
        <Route
          path={RouterPath.PASSWORD_CHANGED}
          element={<PasswordChanged />}
        />
        <Route path={RouterPath.LINK_NOT_VALID} element={<LinkNotValid />} />
        <Route path={RouterPath.LIST_ROUTES}>
          <Route path="" element={
            <PrivateRoute><RoutesList /></PrivateRoute>
          } />
          <Route path={":uuid_route"} element={
            <PrivateRoute><RouteInfo /></PrivateRoute>
          } />
        </Route>
        <Route path={RouterPath.LIST_CLIMBERS}>
          <Route path="" element={
            <PrivateRoute><ClimbersList /></PrivateRoute>
          } />
          <Route path={":uuid_user"} element={
            <PrivateRoute><ClimberInfo /></PrivateRoute>
          } />
        </Route>
        <Route
          path={RouterPath.MY_INFORMATION}
          element={
            <PrivateRoute>
              <Profil />
            </PrivateRoute>
          }
        />
        <Route path="*" element={<LinkNotValid />} />
        <Route path={RouterPath.LIST_NEWS} element={<NewsList />} />
      </Route>
    </Routes>
  </BrowserRouter>,
  rootElement
);

