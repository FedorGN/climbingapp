from typing import Union
from pydantic import BaseModel
from fastapi import Query, Body
from datetime import datetime


class MultipleEventsResponse(BaseModel):
    """The multiple routes response schema."""
    uuid_event: str
    event_type: str
    user_full_name: str
    uuid_user: str
    route_name: Union[str, None]
    uuid_route: Union[str, None]
    grade_route: Union[str, None]
    uuid_followed_user: Union[str, None]
    followed_user_full_name: Union[str, None]
    date_creation: datetime
