from pydantic import BaseModel


class Msg(BaseModel):
    msg: str


class MsgStatus(Msg):
    status: int


class Detail(BaseModel):
    detail: str
