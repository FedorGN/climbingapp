from datetime import datetime
from typing import Optional, Union
from fastapi import Query, Body
from pydantic import BaseModel, EmailStr


# Shared properties
class UserBase(BaseModel):
    email: Optional[EmailStr] = None
    is_active: Optional[bool] = False
    is_superuser: bool = False
    first_name: Optional[str] = None
    last_name: Optional[str] = None


# Properties to receive via API on creation
class UserCreate(UserBase):
    email: EmailStr
    password: str
    first_name: str
    last_name: str
    about: Optional[str]


# Properties to receive via API on update
class UserUpdateInfo(BaseModel):
    first_name: str = Body(...)
    last_name: str = Body(...)
    about: Union[str, None] = Body(default=None)


class UserUpdatePassword(BaseModel):
    password: str


class UserInDBBase(BaseModel):
    email: EmailStr
    first_name: str
    last_name: str

    class Config:
        orm_mode = True


# Additional properties to return via API
class User(UserInDBBase):
    email: EmailStr
    first_name: str
    last_name: str
    about: Union[str, None]
    uuid: str


# Additional properties stored in DB
class UserInDB(UserInDBBase):
    hashed_password: str


class MultipleClimbersFilters(BaseModel):
    """Multiple climbers filters schema."""
    first_name: Union[str, None] = Query(
        default=None, title="First name of the climber.", max_length=150)
    last_name: Union[str, None] = Query(
        default=None, title="Last name of the climber.", max_length=150)
    # climbing_grade: Union[str, None]


class MultipleClimbersResponse(BaseModel):
    """The multiple climbers response schema."""
    uuid_user: str
    user_full_name: str
    count_tops: Union[int, None]
    climbing_grade: Union[str, None]


class MultipleFollowersResponse(BaseModel):
    """The multiple followers/followed response schema."""
    uuid_user: str
    user_full_name: str


class UserInfo(BaseModel):
    """User info schema."""
    uuid: str
    full_name: str
    about: Union[str, None]
    date_creation: datetime
    date_last_login: Union[datetime, None]
    count_tops: Union[int, None]
    climbing_grade: Union[str, None]
    is_followed: Union[bool, None]
    is_me: Union[bool, None]
