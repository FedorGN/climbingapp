from typing import Union
from pydantic import BaseModel
from fastapi import Query, Body
from datetime import datetime


class RouteFullInfo(BaseModel):
    uuid: str = None
    route_name: str
    description: Union[str, None]
    date_start: Union[datetime, None]
    date_end: Union[datetime, None]
    fr_grade: str
    line_name: str
    setter_name: Union[str, None]
    uuid_user: Union[str, None]


class MultipleRouteFilters(BaseModel):  # pylint: disable=too-few-public-methods
    """The filters data schema."""
    name: Union[str, None] = Query(
        default=None, title="Name of the route.", max_length=150)
    grade: Union[str, None] = Query(
        default=None, title="Grade of the route.", max_length=150)
    line: Union[str, None] = Query(
        default=None, title="Line number.", max_length=150)
    uuid_user: Union[str, None] = Query(
        default=None, title="User uuid.", min_length=36, max_length=36)


class MultipleRoutesResponse(BaseModel):
    """The multiple routes response schema."""
    uuid: str
    route_name: str
    grade: str
    line_name: str
    id_user_route: Union[int, None]


class RouteFiltersResponse(BaseModel):
    """The route filters response schema."""
    grades: list[str]
    lines: list[str]


class MultipleRouteTopsResponse(BaseModel):
    """Multiple route tops response schema."""
    user_full_name: str
    uuid_user: str
    date_done: datetime


class UserFollower(BaseModel):
    """Followers response schema."""
    full_name: str
    uuid_user: str


class SingleRouteResponse(BaseModel):
    """Single route response schema."""
    route_name: str
    description: Union[str, None]
    date_start: Union[datetime, None]
    date_end: Union[datetime, None]
    grade: str
    line_name: str
    setter_name: Union[str, None]
    uuid_user: Union[str, None]
    is_done_by_me: bool
    count_done: int
    users: Union[list[UserFollower], None]
