from typing import Any
from fastapi import APIRouter, Depends, Query as QueryAPI, Body, Response, status
from sqlalchemy.orm import Session, Query
from fastapi_pagination import Params, Page
from fastapi_pagination.ext.sqlalchemy import paginate
from app import crud
from app import models
from app import schemas
from app.api import deps

router = APIRouter()


@router.get("/get-routes", response_model=Page[schemas.MultipleRoutesResponse], responses={
    200: {"model": Page[schemas.MultipleRoutesResponse],
          "description": "Succes, list of routes.", },
    401: {"model": schemas.Detail, "description": "User unathorized"},
})
def get_routes(
    filters: schemas.route.MultipleRouteFilters = Depends(
        schemas.route.MultipleRouteFilters),
    current_user: models.User = Depends(deps.get_current_active_user),
    db: Session = Depends(deps.get_db),
    params: Params = Depends(),
) -> Any:
    """Get a list of routes."""
    query: Query = crud.route.query_get_multi(
        db, owner_id=current_user.id, filters=filters)
    return paginate(query, params)


@router.get("/get-routes-filters", response_model=schemas.RouteFiltersResponse, responses={
    200: {"model": schemas.RouteFiltersResponse, "description": "Succes, filters data."},
    401: {"model": schemas.Detail, "description": "User unathorized"}
})
def get_routes_filters(
    current_user: models.User = Depends(deps.get_current_active_user),
    db: Session = Depends(deps.get_db)
) -> Any:
    """Get filter information for routes."""
    filters: schemas.RouteFiltersResponse = crud.route.get_route_filters(db)
    return filters


@router.get("/get-route-info", response_model=schemas.SingleRouteResponse, responses={
    200: {"model": schemas.SingleRouteResponse, "description": "Succes, route information."},
    401: {"model": schemas.Detail, "description": "User unathorized"}
})
def read_route(
    uuid_route: str = QueryAPI(..., min_length=36, max_length=36),
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user)
) -> Any:
    """    Get route information."""
    return crud.route.get_full_info_by_uuid(db, uuid_route=uuid_route, id_user=current_user.id)


@router.get("/get-route-tops", response_model=Page[schemas.MultipleRouteTopsResponse], responses={
    200: {"model": schemas.SingleRouteResponse, "description": "Succes, route information."},
    401: {"model": schemas.Detail, "description": "User unathorized"}
})
def get_route_tops(
    uuid_route: str = QueryAPI(..., min_length=36, max_length=36),
    current_user: models.User = Depends(deps.get_current_active_user),
    db: Session = Depends(deps.get_db),
    params: Params = Depends(),
) -> Any:
    """Get a list of routes tops."""
    query: Query = crud.route.query_get_tops(
        db, uuid_route=uuid_route)
    return paginate(query, params)


@router.post("/user-route-update", response_model=schemas.Msg, responses={
    200: {"model": schemas.Msg, "description": "Link user route deleted."},
    201: {"model": schemas.Msg, "description": "Link user route created."},
    400: {"model": schemas.Msg, "description": "Error."},
})
def update_user_route(
    response: Response,
    is_done: bool = Body(...),
    uuid_route: str = Body(..., min_length=36, max_length=36),
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """Update user route information."""
    result: bool = crud.route.update_user_route(
        db, id_user=current_user.id, is_done=is_done, uuid_route=uuid_route)
    msg: str
    if result is True:
        response.status_code = status.HTTP_201_CREATED
        msg = "User route link created."
    elif result is False:
        response.status_code = status.HTTP_200_OK
        msg = "User route link deleted."
    else:
        response.status_code = status.HTTP_400_BAD_REQUEST
        msg = "Error."
    return {"msg": msg}
