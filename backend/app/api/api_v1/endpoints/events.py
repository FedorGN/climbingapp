from typing import Any
from fastapi import APIRouter, Depends, Query as QueryAPI, Body, Response, status
from sqlalchemy.orm import Session, Query
from fastapi_pagination import Params, Page
from fastapi_pagination.ext.sqlalchemy import paginate
from app import crud
from app import models
from app import schemas
from app.api import deps

router = APIRouter()


@router.get("/get-events", response_model=Page[schemas.MultipleEventsResponse], responses={
    200: {"model": Page[schemas.MultipleEventsResponse],
          "description": "Succes, list of events.", },
    401: {"model": schemas.Detail, "description": "User unathorized"},
})
def get_routes(
    current_user: models.User = Depends(deps.get_current_active_user),
    db: Session = Depends(deps.get_db),
    params: Params = Depends(),
) -> Any:
    """Get a list of routes."""
    query: Query = crud.event.query_get_multi(
        db, current_user=current_user)
    return paginate(query, params)
