from typing import Any, Optional
from fastapi import APIRouter, Body, Depends, HTTPException, Query as QueryAPI
from pydantic.networks import EmailStr
from pydantic import constr
from sqlalchemy.orm import Session, Query
from fastapi_pagination import Params, Page
from app import crud
from app import models
from app import schemas
from app.api import deps
from app.core.config import settings
from app.utils import (send_new_account_email,
                       generate_mail_confirmation_token, verify_mail_confirmation_token)
from fastapi_pagination.ext.sqlalchemy import paginate


router = APIRouter()


@router.get("/get-my-info", response_model=schemas.User, responses={
    200: {"model": schemas.User, "description": "Current user data."},
    401: {"model": schemas.Detail, "description": "User unathorized"},
})
def read_user_me(
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """Get current user information."""
    return current_user


@router.get("/get-user-info", response_model=schemas.UserInfo, responses={
    200: {"model": schemas.User, "description": "User data."},
    401: {"model": schemas.Detail, "description": "User unathorized"},
})
def get_user_info(
    uuid_user: str = QueryAPI(..., min_length=36, max_length=36),
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user)
) -> Any:
    """Get user information."""
    return crud.user.get_user_info(db, uuid_user=uuid_user, current_user=current_user)


@router.post("/signup", response_model=schemas.User, responses={
    400: {"model": schemas.Detail,
          "description": "The user with this username already exists in the system"},
    401: {"model": schemas.Detail, "description": "User unathorized"}
})
def create_user_signup(
    db: Session = Depends(deps.get_db),
    password: str = Body(..., min_length=6, max_length=150),
    email: EmailStr = Body(..., description="Email."),
    first_name: str = Body(..., description="First name.",
                           min_length=1, max_length=150),
    last_name: str = Body(..., description="Last name.",
                          min_length=1, max_length=150),
    about: Optional[str] = Body(
        None, description="About the user.", max_length=300)
) -> Any:
    """User sign up."""
    user = crud.user.get_by_email(db, email=email)
    if user:
        raise HTTPException(
            status_code=400,
            detail="The user with this username already exists in the system",
        )
    user_in = schemas.UserCreate(
        password=password, email=email, first_name=first_name, last_name=last_name, about=about)
    user = crud.user.create(db, obj_in=user_in)
    mail_confirmation_token = generate_mail_confirmation_token(email=email)
    if settings.EMAILS_ENABLED and user_in.email:
        send_new_account_email(email_to=user_in.email,
                               token=mail_confirmation_token)
    return user


@router.post("/update-user-info", response_model=schemas.Msg, responses={
    200: {"model": schemas.Msg,
          "description": "Information updated.", },
    401: {"model": schemas.Detail, "description": "User unathorized"}
})
def update_user_info(
    user_updated: schemas.UserUpdateInfo,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """Update current user information."""
    user = crud.user.get_by_email(db, email=current_user.email)
    user = crud.user.update(db, db_obj=user, obj_in=user_updated)
    return {"msg": "Information updated."}


@router.post("/update-password", response_model=schemas.Msg, responses={
    200: {"model": schemas.Msg,
          "description": "Password updated.", },
    400: {"model": schemas.Detail, "description": "Incorrect old password."}
})
def update_user_password(
    new_password: str = Body(..., min_length=6, max_length=150),
    old_password: str = Body(..., min_length=6, max_length=150),
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """Update current user password."""
    user = crud.user.authenticate(
        db, email=current_user.email, password=old_password
    )
    if not user:
        raise HTTPException(
            status_code=400, detail="Incorrect password")
    user = crud.user.get_by_email(db, email=current_user.email)
    user_updated: schemas.UserUpdatePassword = schemas.UserUpdatePassword(
        password=new_password)
    user = crud.user.update(db, db_obj=user, obj_in=user_updated)
    return {"msg": "Password updated."}


@router.post("/confirm-email/{token}", response_model=schemas.Msg, responses={
    200: {"model": schemas.Msg,
          "description": "Mail confirmed.", },
    400: {"model": schemas.Detail, "description": "Invalid token or mail already confirmed."},
    404: {"model": schemas.Detail, "description": "User not found."},
})
def reset_password(
    *,
    db: Session = Depends(deps.get_db),
    token: str,
) -> Any:
    """Confirm email using token sent in email."""
    email = verify_mail_confirmation_token(token)
    if not email:
        raise HTTPException(status_code=400, detail="Invalid token.")
    user = crud.user.get_by_email(db, email=email)
    if not user:
        raise HTTPException(
            status_code=404,
            detail="The user with this username does not exist in the system.",
        )
    elif crud.user.is_active(user):
        raise HTTPException(
            status_code=400, detail="User mail is already confirmed")
    user.is_active = True
    db.add(user)
    db.commit()
    return {"msg": "Mail confirmed"}


@router.get("/get-climbers", response_model=Page[schemas.MultipleClimbersResponse], responses={
    200: {"model": Page[schemas.MultipleClimbersResponse],
          "description": "Succes, list of climbers.", },
    401: {"model": schemas.Detail, "description": "User unathorized."}
})
def get_climbers(
    current_user: models.User = Depends(deps.get_current_active_user),
    db: Session = Depends(deps.get_db),
    params: Params = Depends(),
    filters: schemas.user.MultipleClimbersFilters = Depends(
        schemas.user.MultipleClimbersFilters)
) -> Any:
    """Get a list of climbers."""
    query: Query = crud.user.query_get_climbers(
        db, filters=filters)
    return paginate(query, params)


@router.post("/follow-user", response_model=schemas.Msg, responses={
    200: {"model": schemas.Msg,
          "description": "Succes, user followed.", },
    400: {"model": schemas.Detail, "description": "Data not modified."},
    401: {"model": schemas.Detail, "description": "User unathorized."}
})
def follow_user(
    uuid_user: str = QueryAPI(..., min_length=36, max_length=36),
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user)
) -> Any:
    """Add user follows another user."""
    res: bool = crud.user.add_follow_user(
        db, uuid_user=uuid_user, current_user=current_user)
    if not res:
        raise HTTPException(
            status_code=400, detail="Data not modified.")
    return {"msg": "User followed."}


@router.delete("/follow-user", response_model=schemas.Msg, responses={
    200: {"model": schemas.Msg,
          "description": "Succes, user unfollowed.", },
    400: {"model": schemas.Detail, "description": "Data not modified."},
    401: {"model": schemas.Detail, "description": "User unathorized."}
})
def unfollow_user(
    uuid_user: str = QueryAPI(..., min_length=36, max_length=36),
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user)
) -> Any:
    """Remove user follows another user."""
    res: bool = crud.user.delete_follow_user(
        db, uuid_user=uuid_user, current_user=current_user)
    if not res:
        raise HTTPException(
            status_code=400, detail="Data not modified.")
    return {"msg": "User unfollowed."}


@router.get("/get-followers", response_model=Page[schemas.MultipleFollowersResponse], responses={
    200: {"model": Page[schemas.MultipleFollowersResponse],
          "description": "Succes, list of followers/following"},
    401: {"model": schemas.Detail, "description": "User unathorized"}
})
def get_followers(
    type: constr(regex="followers|following") = QueryAPI(
        "followers", description="Type of users: followers or following."),
    uuid_user: str = QueryAPI(..., description="User UUID.",
                              min_length=36, max_length=36),
    current_user: models.User = Depends(deps.get_current_active_user),
    db: Session = Depends(deps.get_db),
    params: Params = Depends()
) -> Any:
    """Get a list of followers or followed."""
    query: Query = crud.user.query_get_followers(
        db=db, type=type, uuid_user=uuid_user)
    return paginate(query, params)


@router.delete("/delete-account", response_model=schemas.Msg, responses={
    200: {"model": schemas.Msg,
          "description": "Succes, user deleted.", },
    401: {"model": schemas.Detail, "description": "User unathorized."},
    404: {"model": schemas.Detail, "description": "User not found."}
})
def delete_account(
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user)
) -> Any:
    """Delete current user account."""
    crud.user.delete(db, email=current_user.email)
    return {"msg": "User deleted."}
