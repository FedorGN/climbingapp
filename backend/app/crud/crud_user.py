from typing import Any, Dict, Optional, Union
from datetime import datetime
from sqlalchemy.orm import Session, Query, aliased
from sqlalchemy import func, and_
from fastapi import HTTPException

from app.core.security import get_password_hash, verify_password
from app.crud.base import CRUDBase
from app.db.constants import EventType
from app import schemas
from app.models import User, UserRoute, Grade, Route, UserFollower, Event
from app.schemas.user import UserCreate, UserUpdateInfo, UserUpdatePassword


class CRUDUser(CRUDBase[User, UserCreate, UserUpdateInfo]):
    """CRUD user class."""

    def get_by_email(self, db: Session, *, email: str) -> Union[None, User]:
        """Get user by email."""
        return db.query(User).filter(User.email == email).first()

    def create(self, db: Session, *, obj_in: UserCreate) -> User:
        """Create user."""
        db_obj = User(
            email=obj_in.email,
            hashed_password=get_password_hash(obj_in.password),
            first_name=obj_in.first_name,
            last_name=obj_in.last_name,
            about=obj_in.about,
            is_superuser=obj_in.is_superuser,
            is_active=obj_in.is_active,
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def delete(self, db: Session, email: str) -> bool:
        """Delete user by email."""
        user: User = self.get_by_email(db, email=email)
        if not user:
            raise HTTPException(status_code=404, detail="User not found")
        db.delete(user)
        db.commit()
        return True

    def update(
        self, db: Session, *, db_obj: User, obj_in: Union[UserUpdateInfo, UserUpdatePassword,
                                                          Dict[str, Any]]
    ) -> User:
        """Update user data."""
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict()
        if "password" in update_data:
            hashed_password = get_password_hash(update_data["password"])
            del update_data["password"]
            update_data["hashed_password"] = hashed_password
        return super().update(db, db_obj=db_obj, obj_in=update_data)

    def authenticate(self, db: Session, *, email: str, password: str) -> Optional[User]:
        """Authenticate a user."""
        user: User = self.get_by_email(db, email=email)
        if not user:
            return None
        if not verify_password(password, user.hashed_password):
            return None
        user.date_last_login = datetime.now()
        db.add(user)
        db.commit()
        return user

    def is_active(self, user: User) -> bool:
        """Check if user is active"""
        return user.is_active

    def is_superuser(self, user: User) -> bool:
        """Check if user is superuser"""
        return user.is_superuser

    def query_get_climbers(self, db: Session, filters: schemas.user.MultipleClimbersFilters
                           ) -> Query:
        """Get query list of climbers."""
        subq = (db.query(User.id.label("id_user"), func.max(Grade.fr_grade).label("grade"))
                .outerjoin(UserRoute, User.id == UserRoute.id_user)
                .join(Route, and_(Route.id == UserRoute.id_route, Route.is_active == True))
                .join(Grade, Grade.id == Route.id_grade)
                .group_by(User.id.label("id_user")).subquery())

        query: Query = (
            db.query(User.uuid.label("uuid_user"), User.full_name.label("user_full_name"),
                     subq.c.grade.label("climbing_grade"),
                     func.count(UserRoute.id).label("count_tops"))
            .outerjoin(subq, User.id == subq.c.id_user)
            .outerjoin(UserRoute, UserRoute.id_user == User.id)
            .outerjoin(Route, and_(UserRoute.id_route == Route.id, Route.is_active == True))
            .group_by(User.full_name.label("user_full_name"), User.uuid.label("uuid_user"),
                      subq.c.grade.label("climbing_grade"), User.last_name)
        ).filter(User.is_active == True).order_by(User.last_name)
        if filters.first_name:
            query = query.filter(User.first_name.ilike(
                "%" + filters.first_name + "%"))
        if filters.last_name:
            query = query.filter(User.last_name.ilike(
                "%" + filters.last_name + "%"))
        return query

    def query_get_followers(self, db: Session, type: str, uuid_user: str) -> Query:
        """Get query for followers/following."""
        UserOne = aliased(User)
        UserMany = aliased(User)
        query: Query = db.query(
            UserMany.full_name.label("user_full_name"), UserMany.uuid.label("uuid_user"))
        if type == "followers":
            query = query.join(UserFollower, UserFollower.id_user_follower == UserMany.id).join(
                UserOne, and_(
                    UserOne.id == UserFollower.id_user_followed, UserOne.uuid == uuid_user)
            )
        else:
            query = query.join(UserFollower, UserFollower.id_user_followed == UserMany.id).join(
                UserOne, and_(
                    UserOne.id == UserFollower.id_user_follower, UserOne.uuid == uuid_user)
            )
        query = query.order_by(UserMany.last_name)
        return query

    def get_user_info(self, db: Session, uuid_user: str, current_user: User) -> schemas.UserInfo:
        """Get user information for a given user UUID."""
        subq = (db.query(User.id.label("id_user"), func.max(Grade.fr_grade).label("grade"))
                .outerjoin(UserRoute, User.id == UserRoute.id_user)
                .outerjoin(Route, Route.id == UserRoute.id_route)
                .outerjoin(Grade, Grade.id == Route.id_grade)
                .group_by(User.id.label("id_user"))
                .subquery())

        query: Query = (
            db.query(User.uuid, User.full_name, User.about, User.date_creation,
                     User.date_last_login,
                     func.count((UserRoute.id)).label("count_tops"),
                     subq.c.grade.label("climbing_grade"),
                     UserFollower.id.label("is_followed")
                     )
            .outerjoin(subq, User.id == subq.c.id_user)
            .outerjoin(UserRoute, UserRoute.id_user == User.id)
            .outerjoin(Route, and_(UserRoute.id_route == Route.id, Route.is_active))
            .outerjoin(UserFollower, and_(UserFollower.id_user_followed == User.id,
                                          UserFollower.id_user_follower == current_user.id))
            .filter(User.uuid == uuid_user, User.is_active == True)
            .group_by(User.uuid, User.full_name, User.about, User.date_creation,
                      User.date_last_login,
                      UserFollower.id.label("is_followed"), subq.c.grade.label("climbing_grade"))
        )
        qry_res = query.first()
        if not qry_res:
            raise HTTPException(status_code=404, detail="User not found")
        dict_res: dict = dict(qry_res)
        del dict_res["is_followed"]
        user_info: schemas.UserInfo = schemas.UserInfo(**dict_res)
        user_info.is_me = False
        user_info.is_followed = False
        if qry_res.is_followed:
            user_info.is_followed = True
        if qry_res.uuid == current_user.uuid:
            user_info.is_me = True
        return user_info

    def add_follow_user(self, db: Session, uuid_user: str, current_user: User) -> bool:
        """Add current user follows the given user."""
        user_info = db.query(User.id).filter(User.uuid == uuid_user).first()
        if not user_info:
            return False
        user_follower: UserFollower = (db.query(UserFollower)
                                       .join(User, and_(User.id == UserFollower.id_user_followed,
                                                        User.uuid == uuid_user))
                                       .filter(UserFollower.id_user_follower == current_user.id)
                                       .first())
        if user_follower:
            return False
        user_follower = UserFollower(id_user_followed=user_info.id,
                                     id_user_follower=current_user.id)
        event = Event(event_type=EventType.USER_FOLLOWED_USER.value, user_follower=user_follower,
                      id_user=current_user.id, description="User followed user.")
        db.add(user_follower, event)
        db.commit()
        return True

    def delete_follow_user(self, db: Session, uuid_user: str, current_user: User) -> bool:
        """Add current user follows the given user."""
        user_follower = (db.query(UserFollower)
                         .join(User, and_(User.id == UserFollower.id_user_followed,
                                          User.uuid == uuid_user))
                         .filter(UserFollower.id_user_follower == current_user.id).first())
        if not user_follower:
            return False
        db.delete(user_follower)
        db.commit()
        return True


user = CRUDUser(User)
