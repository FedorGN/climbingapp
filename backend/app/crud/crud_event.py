from sqlalchemy.orm import Session, Query
from sqlalchemy import and_

from app.crud.base import CRUDBase
from app.models import Route, Grade, UserRoute, User, UserFollower, Event


class CRUDEvent(CRUDBase):
    """CRUD event class."""

    def query_get_multi(
        self, db: Session, current_user: User,
    ) -> Query:

        subq = (db.query(User.id.label("id_user"), User.full_name, User.uuid)
                .join(UserFollower, and_(User.id == UserFollower.id_user_followed,
                                         UserFollower.id_user_follower == current_user.id))
                .subquery())

        query: Query = (
            db.query(Event.uuid.label("uuid_event"), Event.event_type,
                     Event.date_creation,
                     subq.c.full_name.label("user_full_name"), subq.c.uuid.label(
                         "uuid_user"), Route.name.label("route_name"),
                     Route.uuid.label("uuid_route"), User.uuid.label(
                         "uuid_followed_user"),
                     User.full_name.label("followed_user_full_name"),
                     Grade.fr_grade.label("grade_route"))
            .join(subq, Event.id_user == subq.c.id_user)
            .outerjoin(UserRoute, UserRoute.id == Event.id_user_route)
            .outerjoin(Route, Route.id == UserRoute.id_route)
            .outerjoin(Grade, Grade.id == Route.id_grade)
            .outerjoin(UserFollower, UserFollower.id == Event.id_user_follower)
            .outerjoin(User, UserFollower.id_user_followed == User.id)
            .filter()
        )

        return query


event = CRUDEvent(Route)
