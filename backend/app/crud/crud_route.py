from typing import Union
from datetime import datetime
from sqlalchemy.orm import Session, Query, aliased
from sqlalchemy import and_, func

from app.crud.base import CRUDBase
from app.models import Route, Grade, Line, UserRoute, User, UserFollower, Event
from app.db.constants import EventType
from app import schemas


class CRUDRoute(CRUDBase):
    """CRUD route class."""

    def get_by_uuid(self, db: Session, uuid: str) -> Union[Route, None]:
        """Get route by given uuid."""
        return db.query(Route).filter(Route.uuid == uuid).first()

    def get_full_info_by_uuid(self, db: Session, uuid_route: str, id_user: int
                              ) -> Union[None, schemas.SingleRouteResponse]:
        """Get full route information."""
        UserRouteCurrentUser = aliased(UserRoute)

        route = (
            db.query(
                Route.name.label("route_name"), Route.description,
                Route.date_start, Route.date_end,
                Grade.fr_grade.label("grade"), Line.name.label("line_name"),
                User.full_name.label(
                    "setter_name"), User.uuid.label("uuid_user"),
                UserRouteCurrentUser.id.label("is_done_by_me"),
                func.count(UserRoute.id).label("count_done"))
            .join(Grade, Route.id_grade == Grade.id)
            .join(Line, Route.id_line == Line.id)
            .outerjoin(User, Route.id_user_setter == User.id)
            .outerjoin(UserRoute, and_(Route.id == UserRoute.id_route))
            .outerjoin(UserRouteCurrentUser, and_(
                Route.id == UserRouteCurrentUser.id_route,
                UserRouteCurrentUser.id_user == id_user))
            .filter(Route.uuid == uuid_route)
            .group_by(Route.name.label("route_name"), Route.description,
                      Route.date_start, Route.date_end,
                      Grade.fr_grade.label("grade"),
                      Line.name.label("line_name"),
                      User.full_name.label("setter_name"),
                      User.uuid.label("uuid_user"),
                      UserRouteCurrentUser.id.label("is_done_by_me"))
            .first()
        )
        climbers = (db.query(User.full_name, User.uuid.label("uuid_user"))
                    .join(UserRoute, UserRoute.id == User.id)
                    .join(UserFollower, and_(UserFollower.id_user_followed == User.id,
                                             UserFollower.id_user_follower == id_user))
                    # .limit(5)
                    .all()
                    )
        route_obj: schemas.SingleRouteResponse = schemas.SingleRouteResponse(
            route_name=route.route_name,
            description=route.description,
            date_start=route.date_start,
            date_end=route.date_end,
            grade=route.grade,
            line_name=route.line_name,
            setter_name=route.setter_name,
            uuid_user=route.uuid_user,
            is_done_by_me=True if route.is_done_by_me else False,
            count_done=route.count_done,
            users=climbers
        )
        return route_obj

    def query_get_multi(
        self, db: Session, *, owner_id: int, filters: schemas.route.MultipleRouteFilters
    ) -> Query:
        query: Query = (
            db.query(Route.uuid, Route.name.label("route_name"),
                     Grade.fr_grade.label(
                         "grade"), Line.name.label("line_name"),
                     UserRoute.id.label("id_user_route"))
        )
        if filters.name:
            query = query.filter(Route.name.ilike("%" + filters.name + "%"))
        if filters.grade:
            query = query.filter(Grade.fr_grade.ilike(filters.grade))
        if filters.line:
            query = query.filter(Line.name.ilike(filters.line))
        if filters.uuid_user:
            query = query.join(UserRoute, UserRoute.id_route == Route.id
                               ).join(User, UserRoute.id_user == User.id
                                      ).filter(User.uuid == filters.uuid_user)
        else:
            query = query.outerjoin(UserRoute, and_(
                UserRoute.id_route == Route.id, UserRoute.id_user == owner_id))

        query = (query.join(Grade, Grade.id == Route.id_grade)
                 .join(Line, Line.id == Route.id_line)
                 .group_by(Route.uuid, Route.name.label("route_name"),
                           Grade.fr_grade.label(
                     "grade"), Line.name.label("line_name"))
                 .filter(Route.is_active)
                 .order_by(Line.id)
                 .group_by(Route.uuid, Route.name.label("route_name"),
                           Grade.fr_grade.label(
                     "grade"), Line.name.label("line_name"),
                     UserRoute.id.label("id_user_route"), Line.id)
                 )
        return query

    def query_get_tops(self, db: Session, uuid_route: str) -> Query:
        """Get query route tops for given UUID route."""
        query: Query = (
            db.query(User.full_name.label("user_full_name"), User.uuid.label(
                "uuid_user"), UserRoute.date_done)
            .join(UserRoute, User.id == UserRoute.id_user)
            .join(Route, UserRoute.id_route == Route.id)
            .filter(Route.uuid == uuid_route)
        )
        return query

    def get_route_filters(self, db: Session) -> schemas.route.RouteFiltersResponse:
        """Get filters for route list."""
        grades: list[str] = [
            res.fr_grade for res in db.query(Grade.fr_grade).all()]
        lines: list[str] = [res.name for res in db.query(Line.name).all()]
        return schemas.route.RouteFiltersResponse(grades=grades, lines=lines)

    def update_user_route(self, db: Session, id_user: int, is_done: bool, uuid_route: str
                          ) -> Union[bool, None]:
        """Update User Route information."""
        user_route = (
            db.query(UserRoute).join(Route, and_(
                UserRoute.id_route == Route.id, Route.uuid == uuid_route))
            .filter(UserRoute.id_user == id_user).first()
        )
        if user_route and not is_done:
            db.delete(user_route)
            db.commit()
            return False
        elif not user_route and is_done:
            query_res = db.query(Route.id).filter(
                Route.uuid == uuid_route).first()
            if not route:
                return None
            user_route = UserRoute(
                id_user=id_user,
                id_route=query_res.id,
                date_done=datetime.today()
            )
            event = Event(event_type=EventType.USER_SENT_ROUTE.value, user_route=user_route,
                          id_user=id_user, description="User sent the route.")
            db.add(user_route, event)
            db.commit()
            return True
        return None


route = CRUDRoute(Route)
