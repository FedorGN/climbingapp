from enum import Enum


class EventType(Enum):
    USER_SENT_ROUTE = "eUserSentRoute"
    USER_FOLLOWED_USER = "eUserFollowedUser"
