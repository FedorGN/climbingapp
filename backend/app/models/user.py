from uuid import uuid4
from datetime import datetime
from sqlalchemy import Boolean, Column, Integer, String, DateTime, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property
from app.db.base_class import Base


class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String(500), unique=True, nullable=False)
    first_name = Column(String(150), index=True)
    last_name = Column(String(150), index=True)
    email = Column(String(150), unique=True, index=True, nullable=False)
    hashed_password = Column(String, nullable=False)
    about = Column(String(300), nullable=True)
    is_active = Column(Boolean(), default=False)
    is_superuser = Column(Boolean(), default=False)
    role = Column(String(255))
    date_creation = Column(DateTime, nullable=False)
    date_last_login = Column(DateTime, nullable=True)
    date_pwd_reinit = Column(DateTime, nullable=True)
    user_routes = relationship(
        "UserRoute", cascade="all,delete", backref="user", foreign_keys="[UserRoute.id_user]")
    users_followed = relationship(
        "UserFollower", cascade="all,delete", foreign_keys="[UserFollower.id_user_follower]")
    users_followers = relationship(
        "UserFollower", cascade="all,delete", foreign_keys="[UserFollower.id_user_followed]")
    events = relationship(
        "Event", cascade="all,delete", backref="user", foreign_keys="[Event.id_user]")

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        self.date_creation = datetime.now()
        self.uuid = str(uuid4())
        self.last_name = self.last_name.upper()
        self.first_name = self.first_name.title()

    @hybrid_property
    def full_name(self):
        return (" ".join([self.first_name, self.last_name]))

    @full_name.expression
    def full_name(cls):
        return cls.first_name + " " + cls.last_name
