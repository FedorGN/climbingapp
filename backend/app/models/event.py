from datetime import datetime
from uuid import uuid4

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime

from app.db.base_class import Base
from app.db.constants import EventType


class Event(Base):
    __tablename__ = "event"
    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String(100), unique=True, nullable=False)
    description = Column(String(3000), nullable=True)
    event_type = Column(String(30), nullable=False)
    id_user_route = Column(Integer, ForeignKey("user_route.id"), nullable=True)
    id_user_follower = Column(Integer, ForeignKey(
        "user_follower.id"), nullable=True)
    id_user = Column(Integer, ForeignKey("user.id"), nullable=False)
    date_creation = Column(DateTime, nullable=False)

    def __init__(self, **kwargs):
        if kwargs.get('event_type') not in [v.value for v in EventType]:
            raise Exception(
                "Event type is not in the list of authorized event types.")
        super(Event, self).__init__(**kwargs)
        self.date_creation = datetime.now()
        self.uuid = str(uuid4())
