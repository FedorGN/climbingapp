from datetime import datetime
from uuid import uuid4
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class UserRoute(Base):
    __tablename__ = "user_route"
    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String(100), unique=True, nullable=False)
    id_user = Column(Integer, ForeignKey("user.id"), nullable=False)
    id_route = Column(Integer, ForeignKey("route.id"), nullable=False)
    date_done = Column(DateTime, nullable=False)
    date_creation = Column(DateTime, nullable=False)

    events = relationship(
        "Event", cascade="all,delete", backref="user_route", foreign_keys="[Event.id_user_route]")

    def __init__(self, **kwargs):
        super(UserRoute, self).__init__(**kwargs)
        self.date_creation = datetime.now()
        self.uuid = str(uuid4())
