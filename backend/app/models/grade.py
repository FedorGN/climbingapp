from datetime import datetime
from uuid import uuid4

from sqlalchemy import Column, Integer, String

from app.db.base_class import Base


class Grade(Base):
    __tablename__ = "grade"
    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String(100), unique=True, nullable=False)
    fr_grade = Column(String(150))
    usa_grade = Column(String(150))

    def __init__(self, **kwargs):
        super(Grade, self).__init__(**kwargs)
        self.date_creation = datetime.now()
        self.uuid = str(uuid4())
