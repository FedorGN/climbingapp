from datetime import datetime
from uuid import uuid4

from sqlalchemy import Boolean, Column, Integer, String, ForeignKey, DateTime

from app.db.base_class import Base


class Route(Base):
    __tablename__ = "route"
    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String(100), unique=True, nullable=False)
    name = Column(String(150), index=True)
    description = Column(String(1000), nullable=True)
    id_line = Column(Integer, ForeignKey("line.id"), nullable=False)
    id_grade = Column(Integer, ForeignKey("grade.id"), nullable=False)
    id_user_setter = Column(Integer, ForeignKey("user.id"), nullable=True)
    is_active = Column(Boolean, default=False)
    date_start = Column(DateTime, nullable=True)
    date_end = Column(DateTime, nullable=True)

    def __init__(self, **kwargs):
        super(Route, self).__init__(**kwargs)
        self.date_creation = datetime.now()
        self.uuid = str(uuid4())
