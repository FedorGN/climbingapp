from datetime import datetime
from uuid import uuid4
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class UserFollower(Base):
    __tablename__ = "user_follower"
    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String(100), unique=True, nullable=False)
    id_user_follower = Column(Integer, ForeignKey("user.id"), nullable=False)
    id_user_followed = Column(Integer, ForeignKey("user.id"), nullable=False)
    date_creation = Column(DateTime, nullable=False)

    events = relationship("Event", cascade="all,delete", backref="user_follower",
                          foreign_keys="[Event.id_user_follower]")

    def __init__(self, **kwargs):
        super(UserFollower, self).__init__(**kwargs)
        self.date_creation = datetime.now()
        self.uuid = str(uuid4())
