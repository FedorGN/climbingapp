from datetime import datetime
from uuid import uuid4

from sqlalchemy import Boolean, Column, Integer, String

from app.db.base_class import Base


class Line(Base):
    __tablename__ = "line"
    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String(100), unique=True, nullable=False)
    is_active = Column(Boolean(), default=True)
    name = Column(String(150), index=True)

    def __init__(self, **kwargs):
        super(Line, self).__init__(**kwargs)
        self.date_creation = datetime.now()
        self.uuid = str(uuid4())
